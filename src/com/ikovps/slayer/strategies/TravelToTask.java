package com.ikovps.slayer.strategies;

import org.parabot.environment.scripts.framework.Strategy;

import com.ikovps.slayer.data.Variables;

/**
 * A class that teleports and walks to an assigned Slayer task.
 * 
 * @author Masood && El Maestro
 * 
 */
public class TravelToTask implements Strategy {

	@Override
	public boolean activate() {
		if (Variables.getTask() == null) {
			return false;
		}
		return !Variables.getTask().isDone() && !Variables.getTask().atTask() && Variables.getTask().getPath().isTraversable();
	}

	@Override
	public void execute() {
		Variables.getTask().travelTo();
	}
}