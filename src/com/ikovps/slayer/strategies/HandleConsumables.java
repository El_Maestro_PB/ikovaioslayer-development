package com.ikovps.slayer.strategies;

import java.util.ArrayList;
import java.util.Map.Entry;

import org.parabot.environment.scripts.framework.Strategy;

import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.util.Consumable;
import com.ikovps.slayer.util.Utils;

public class HandleConsumables implements Strategy {
	private ArrayList<Consumable> consumables = new ArrayList<Consumable>();

	@Override
	public boolean activate() {
		if (Variables.getConsumables().isEmpty()) {
			return false;
		}
		if (Variables.getTask() == null) {
			return false;
		}
		for (Entry<Consumable, Integer[]> entry : Variables.getConsumables().entrySet()) {
			if (entry.getKey().needToConsume() && entry.getKey().hasInBag()
					&& (Utils.isFighting() || Variables.getTask() != null && Variables.getTask().atTask())) {
				consumables.add(entry.getKey());
			}
		}
		return consumables.isEmpty() ? false : true;
	}

	@Override
	public void execute() {
		for (Consumable consumable : consumables) {
			Variables.setStatus("Consuming " + consumable.getName() + "...", false);
			consumable.consume();
		}
		consumables.clear();
	}

}
