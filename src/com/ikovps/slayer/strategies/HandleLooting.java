package com.ikovps.slayer.strategies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.GroundItems;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.GroundItem;

import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.util.Utils;

public class HandleLooting implements Strategy {

	@Override
	public boolean activate() {
		GroundItem[] toPickup = GroundItems.getNearest(Variables.getLoot());
		return !Utils.isWalking() && Players.getMyPlayer().getInteractingCharacter() == null && hasSpace() && toPickup.length > 0
				&& toPickup[0] != null && toPickup[0].distanceTo() <= 6 && Variables.getTask() != null
				&& Variables.getTask().atTask();
	}

	@Override
	public void execute() {
		Variables.setStatus("Picking up loot...", true);
		final GroundItem[] toPickup = GroundItems.getNearest(Variables.getLoot());
		if (Inventory.isFull() && Variables.getFood().getCount() >= 1) {
			Variables.getFood().consume();
		}
		if (toPickup.length > 0 && toPickup[0] != null) {
			Menu.sendAction(234, toPickup[0].getId(), toPickup[0].getX(), toPickup[0].getY());
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return !Utils.isWalking() && (toPickup[0] == null || toPickup.length < 1);
				}
			}, 2000);
		}
	}

	private boolean hasSpace() {
		return Inventory.getCount() <= 27 || Inventory.isFull() && Variables.getFood().getCount() >= 1;
	}

}
