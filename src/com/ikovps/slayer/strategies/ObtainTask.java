package com.ikovps.slayer.strategies;

import org.parabot.environment.scripts.framework.Strategy;

import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.util.Utils;

public class ObtainTask implements Strategy {

	@Override
	public boolean activate() {
		if (Variables.getTask() == null) {
			return true;
		}
		return !Utils.isFighting() && (Variables.isResetTask() || Variables.getTask().isDone());
	}

	@Override
	public void execute() {
		Variables.setStatus("Getting a task...", true);
		if (Variables.getMaster().atMaster()) {
			Variables.getMaster().getTask();
		} else {
			Variables.getMaster().travelTo();
		}
	}
}