package com.ikovps.slayer.strategies;

import java.util.Map.Entry;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Bank;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Item;

import com.ikovps.slayer.data.Constants;
import com.ikovps.slayer.data.Food;
import com.ikovps.slayer.data.Potion;
import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.util.Consumable;
import com.ikovps.slayer.util.Teleports;
import com.ikovps.slayer.util.Utils;

/**
 * A class that banks.
 * 
 * @author Masood && El Maestro
 * 
 */

public class HandleBanking implements Strategy {
	private boolean needConsumable;

	@Override
	public boolean activate() {
		if (!Variables.getConsumables().isEmpty()) {
			for (Entry<Consumable, Integer[]> entry : Variables.getConsumables().entrySet()) {
				if (entry.getKey().needBankForMore()) {
					needConsumable = true;
					return !Utils.isFighting();
				}
			}
		}
		return Inventory.isFull() && Inventory.contains(Variables.getLootInInventory()) && Variables.getFood().getCount() < 1
				|| Utils.needAntiShield() && Variables.getTask() != null && Variables.getTask().requiresDragonShield()
				|| Bank.isOpen();

	}

	@Override
	public void execute() {
		HandleCombat.monsterToFight = null;
		Variables.setStatus("Banking...", true);
		if (Bank.getNearestBanks().length == 0) {
			Teleports.HOME.teleport();
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return Bank.getNearestBanks().length > 1;
				}
			}, 3000);
			return;
		}
		if (!Bank.isOpen()) {
			while (!Bank.isOpen()) {
				Bank.open(Bank.getBank());
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Bank.isOpen();
					}
				}, 3000);
			}
		}
		if (Bank.isOpen()) {
			if (Inventory.getItems(Variables.getLootInInventory()) != null) {
				for (Item loot : Inventory.getItems(Variables.getLoot())) {
					if (loot != null) {
						Menu.sendAction(431, loot.getId() - 1, loot.getSlot(), 5064);
						Time.sleep(500);
					}
				}
			}
			if (needConsumable) {
				for (Entry<Consumable, Integer[]> entry : Variables.getConsumables().entrySet()) {
					if (entry.getKey().getCount() < entry.getValue()[0]) {
						if (entry.getKey() instanceof Potion) {
							final Potion potion = (Potion) entry.getKey();
							Bank.withdraw(potion.getIdByDose(potion.isFlask() ? 6 : 3), (entry.getValue()[0] - entry.getKey()
									.getCount()), 500);
						} else {
							final Food food = (Food) entry.getKey();
							Bank.withdraw(food.getId(), (entry.getValue()[0] - entry.getKey().getCount()), 500);
						}
						Time.sleep(2000);
					}
				}
				needConsumable = false;
			}
			if (Variables.getTask() != null) {
				if (Utils.needAntiShield() && Variables.getTask().requiresDragonShield()) {
					Variables.setStatus("Withdrawing anti-dragon shield...", false);
					for (int o = 0; o < Constants.ANTI_FIRESHIELDS.length; o++) {
						if (Bank.getItem(Constants.ANTI_FIRESHIELDS[o]) != null) {
							Bank.withdraw(Constants.ANTI_FIRESHIELDS[o], 1, 500);
							Time.sleep(2000);
							break;
						}
					}
					if (Utils.needAntiShield()) {
						Variables.setStatus("No anti-fireshield found for " + Variables.getTask().getMonsterName() + " task.",
								false);
						Variables.setTask(null);
					}
				}
			}
			Bank.close();
		}
	}

}