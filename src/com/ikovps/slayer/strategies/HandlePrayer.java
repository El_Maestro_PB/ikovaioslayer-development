package com.ikovps.slayer.strategies;

import org.parabot.environment.scripts.framework.Strategy;

import com.ikovps.slayer.data.Prayer;
import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.util.Utils;

public class HandlePrayer implements Strategy {

	@Override
	public boolean activate() {
		if (Variables.getTask() == null) {
			return false;
		}
		if (Variables.isPray()) {
			return !Prayer.areActivated() && !Variables.getTask().isDone()
					&& (Utils.isFighting() || Variables.getTask().atTask()) || Prayer.areActivated()
					&& (Variables.getTask().isDone() || !Variables.getTask().atTask());
		}
		return false;
	}

	@Override
	public void execute() {
		Variables.setStatus("Toggling prayers...", false);
		if (!Prayer.areActivated()) {
			Prayer.activate();
		} else {
			Prayer.unActivate();
		}
	}
}
