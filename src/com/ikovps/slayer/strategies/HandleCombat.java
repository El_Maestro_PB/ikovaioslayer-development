package com.ikovps.slayer.strategies;

import java.util.Map.Entry;

import org.parabot.environment.api.utils.Filter;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Npc;

import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.util.Consumable;
import com.ikovps.slayer.util.Equipment;
import com.ikovps.slayer.util.Utils;

/**
 * A class that attacks assigned Slayer monsters.
 * 
 * @author Masood && El Maestro
 * 
 */
public class HandleCombat implements Strategy {
	public static Npc monsterToFight = null;
	private boolean bugged = false;

	@Override
	public boolean activate() {
		if (Variables.getTask().atTask()) {
			return !Utils.isWalking() && !Utils.isFighting();
		}
		return false;
	}

	@Override
	public void execute() {
		if (!Utils.retaliateIsActivated()) {
			Variables.setStatus("Turning on auto retaliate...", false);
			Utils.toggleRetaliate();
		}
		if (Variables.isSpecialAttack()) {
			if (Utils.canSpecial() && !Utils.isSpecActivated()) {
				Variables.setStatus("Turning special attack on...", false);
				Utils.handleSpecialAttack();
			} else if (!Utils.canSpecial() && Equipment.WEAPON.isWearing(Variables.getSpecialWeapon())) {
				Variables.setStatus("Equiping primary weapon...", false);
				Equipment.WEAPON.equip(Variables.getPrimaryWeapon());
			}
		}
		if (monsterToFight != null) {
			while (!needToConsume() && monsterToFight != null && Game.isLoggedIn() && Variables.getTask().atTask()) {
				Variables.setStatus("Fighting " + Variables.getTask().getMonsterName() + "...", true);
				if (!bugged && Players.getMyPlayer().getInteractingCharacter() != null) {
					if (!monsterToFight.equals(Players.getMyPlayer().getInteractingCharacter())) {
						bugged = true;
					}
				}
				if (monsterToFight.isInCombat() && monsterToFight.getHealth() == 0
						|| Utils.npcIsInCombatWithOther(monsterToFight) && !Players.getMyPlayer().isInCombat()
						|| !Utils.isFighting() && bugged) {
					bugged = false;
					monsterToFight = null;
					break;
				}
				if (!Players.getMyPlayer().isInCombat() || !Utils.isFighting()) {
					monsterToFight.interact(1);
				}
				if (Players.getMyPlayer().getInteractingCharacter() != null) {
					if (!Players.getMyPlayer().getInteractingCharacter().equals(monsterToFight)
							&& Players.getMyPlayer().isInCombat()) {
						monsterToFight = (Npc) Players.getMyPlayer().getInteractingCharacter();
					}
				}
				if (Variables.isSpecialAttack()) {
					if (Utils.canSpecial() && !Utils.isSpecActivated()) {
						Variables.setStatus("Turning special attack on...", false);
						Utils.handleSpecialAttack();
						monsterToFight.interact(1);
					} else if (!Utils.canSpecial() && Equipment.WEAPON.isWearing(Variables.getSpecialWeapon())) {
						Variables.setStatus("Equiping primary weapon...", false);
						Equipment.WEAPON.equip(Variables.getPrimaryWeapon());
						monsterToFight.interact(1);
					}
				}
				Time.sleep(1000, 2000);
			}
		} else {
			final Npc[] monsters = Npcs.getNearest(slayerMonsterFilter);
			if (monsters.length > 0) {
				monsterToFight = monsters[0];
			}
		}
	}

	private final Filter<Npc> slayerMonsterFilter = new Filter<Npc>() {
		@Override
		public boolean accept(Npc n) {
			for (int target : Variables.getTask().getNpcIds()) {
				if (n != null && n.getDef().getId() == target && !n.isInCombat() && n.getInteractingCharacter() == null
						&& n.getLocation().isReachable()) {
					return true;
				}
			}
			return false;
		}
	};

	private boolean needToConsume() {
		if (Variables.getConsumables().isEmpty()) {
			return false;
		}
		for (Entry<Consumable, Integer[]> entry : Variables.getConsumables().entrySet()) {
			if (entry.getKey().needToConsume()) {
				return true;
			}
		}
		return false;
	}

}