package com.ikovps.slayer.strategies;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.parabot.environment.scripts.framework.Strategy;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Item;

import com.ikovps.slayer.data.Constants;
import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.util.Equipment;
import com.ikovps.slayer.util.Utils;

public class ManageInventory implements Strategy {
	@Override
	public boolean activate() {
		return !Utils.isFighting()
				&& (Inventory.getCount(Constants.VIAL) > 0 || Inventory.contains(Constants.CASKET) || Inventory
						.contains(Constants.COINS))
				|| Inventory.contains(Constants.ANTI_FIRESHIELDS)
				&& Variables.getTask() != null
				&& Variables.getTask().requiresDragonShield()
				&& (!Equipment.SHIELD.isWearing(Constants.ANTI_FIRESHIELDS[0]) || !Equipment.SHIELD
						.isWearing(Constants.ANTI_FIRESHIELDS[1]));
	}

	@Override
	public void execute() {
		if (Variables.getTask().requiresDragonShield()) {
			if (Inventory.getItems(Constants.ANTI_FIRESHIELDS) != null
					&& Variables.getTask() != null
					&& (!Equipment.SHIELD.isWearing(Constants.ANTI_FIRESHIELDS[0]) || !Equipment.SHIELD
							.isWearing(Constants.ANTI_FIRESHIELDS[1]))) {
				for (int o = 0; o < Constants.ANTI_FIRESHIELDS.length; o++) {
					if (Inventory.getItem(Constants.ANTI_FIRESHIELDS[o]) != null) {
						Variables.setStatus("Equiping anti-fireshield...", false);
						Equipment.SHIELD.equip(Constants.ANTI_FIRESHIELDS[o]);
						return;
					}
				}
				return;
			} else if (Variables.getTask().isDone()
					&& !Variables.getTask().atTask()
					&& (Equipment.SHIELD.isWearing(Constants.ANTI_FIRESHIELDS[0]) || Equipment.SHIELD
							.isWearing(Constants.ANTI_FIRESHIELDS[1]))) {
				Variables.setStatus("Removing anti-fireshield...", false);
				Equipment.SHIELD.remove();
			}
		}
		if (Inventory.contains(Constants.CASKET)) {
			Variables.setStatus("Opening casket...", false);
			Item casket = Inventory.getItem(Constants.CASKET);
			Menu.sendAction(74, Constants.CASKET - 1, casket.getSlot(), 3214);
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return !Inventory.contains(Constants.CASKET);
				}
			}, 1000);
		}
		if (Inventory.contains(Constants.COINS)) {
			Variables.setStatus("Putting coins in money pouch...", false);
			Item coins = Inventory.getItem(Constants.COINS);
			Menu.sendAction(493, Constants.COINS - 1, coins.getSlot(), 3214);
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return !Inventory.contains(Constants.COINS);
				}
			}, 1000);
		}
		for (Item item : Inventory.getItems(Constants.VIAL)) {
			item.drop();
			Time.sleep(1000);
		}
	}
}
