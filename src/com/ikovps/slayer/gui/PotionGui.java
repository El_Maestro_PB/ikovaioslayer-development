package com.ikovps.slayer.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class PotionGui extends JFrame {

	private static final long serialVersionUID = 4881430762375024140L;

	private JPanel contentPane;
	private final JSpinner amountSpinner = new JSpinner();
	private final JComboBox comboBox = new JComboBox();

	/**
	 * Create the frame.
	 */
	public PotionGui() {
		setResizable(false);
		setTitle("Potion Selecter");
		setBounds(100, 100, 299, 138);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblFoodType = new JLabel("Potion Type");
		lblFoodType.setBounds(51, 11, 72, 14);
		contentPane.add(lblFoodType);

		JLabel lblAmount = new JLabel("Amount to Withdraw");
		lblAmount.setBounds(169, 11, 114, 14);
		contentPane.add(lblAmount);
		comboBox.setToolTipText("Potion type.");

		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Super Strength Potion", "Super Strength Flask",
				"Super Attack Potion", "Super Attack Flask", "Super Defense Potion", "Super Defense Flask", "Prayer Potion",
				"Prayer Flask", "Prayer Renewal Potion", "Prayer Renewal Flask", "Super Restore Potion", "Super Restore Flask" }));
		comboBox.setBounds(10, 41, 137, 20);
		contentPane.add(comboBox);
		amountSpinner.setToolTipText("Amount of potion to withdraw from bank.");

		amountSpinner.setModel(new SpinnerNumberModel(1, 1, 28, 1));
		amountSpinner.setBounds(198, 41, 44, 20);
		contentPane.add(amountSpinner);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
				Gui.addConsumable("POTION:" + comboBox.getSelectedItem().toString() + ":" + amountSpinner.getValue().toString());
				amountSpinner.setValue(1);
				comboBox.setSelectedIndex(0);
			}
		});
		btnAdd.setBounds(108, 76, 89, 23);
		contentPane.add(btnAdd);
	}

}
