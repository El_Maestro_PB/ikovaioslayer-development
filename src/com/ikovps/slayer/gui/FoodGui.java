package com.ikovps.slayer.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;

public class FoodGui extends JFrame {

	private static final long serialVersionUID = -166935508521977102L;

	private JPanel contentPane;
	private final JSpinner eatAtSpinner = new JSpinner();
	private final JSpinner amountSpinner = new JSpinner();
	@SuppressWarnings("rawtypes")
	private final JComboBox comboBox = new JComboBox();

	/**
	 * Create the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public FoodGui() {
		setResizable(false);
		setTitle("Food Selecter");
		setBounds(100, 100, 344, 138);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblFoodType = new JLabel("Food Type");
		lblFoodType.setBounds(10, 11, 72, 14);
		contentPane.add(lblFoodType);

		JLabel lblAmount = new JLabel("Amount to Withdraw");
		lblAmount.setBounds(111, 11, 103, 14);
		contentPane.add(lblAmount);

		JLabel lblHealthTo = new JLabel("Health % to Eat At");
		lblHealthTo.setBounds(237, 11, 91, 14);
		contentPane.add(lblHealthTo);
		comboBox.setToolTipText("Food type.");

		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Lobster", "Monkfish", "Shark", "Rocktail" }));
		comboBox.setBounds(10, 41, 72, 20);
		contentPane.add(comboBox);

		amountSpinner.setModel(new SpinnerNumberModel(1, 1, 28, 1));
		amountSpinner.setBounds(144, 41, 44, 20);
		contentPane.add(amountSpinner);

		eatAtSpinner.setModel(new SpinnerNumberModel(0, 0, 100, 5));
		eatAtSpinner.setBounds(259, 41, 47, 20);
		contentPane.add(eatAtSpinner);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Gui.addConsumable("FOOD:" + comboBox.getSelectedItem().toString() + ":" + amountSpinner.getValue().toString()
						+ ":" + eatAtSpinner.getValue().toString());
				eatAtSpinner.setValue(0);
				amountSpinner.setValue(1);
				comboBox.setSelectedIndex(0);
			}
		});
		btnAdd.setBounds(125, 76, 89, 23);
		contentPane.add(btnAdd);
	}

}
