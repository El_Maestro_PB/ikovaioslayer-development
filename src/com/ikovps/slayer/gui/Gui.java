package com.ikovps.slayer.gui;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.ikovps.slayer.data.Food;
import com.ikovps.slayer.data.Potion;
import com.ikovps.slayer.data.Prayer;
import com.ikovps.slayer.data.SlayerMaster;
import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.util.Utils;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class Gui extends JFrame {

	private static final long serialVersionUID = -8135224636266383486L;

	private final JPanel contentPane;
	private final JList<String> consumables = new JList<String>(consumablesListModel);
	private final ButtonGroup modeButtonGroup = new ButtonGroup();
	private final ButtonGroup slayerMasterButtonGroup = new ButtonGroup();
	private final JRadioButton campMonster = new JRadioButton("Camp a Monster");
	private final JRadioButton slayer = new JRadioButton("Train Slayer");
	private final JRadioButton mazchna = new JRadioButton("Mazachna");
	private final JRadioButton vannaka = new JRadioButton("Vannaka");
	private final JRadioButton duradel = new JRadioButton("Duradel");
	private final JRadioButton chaeldar = new JRadioButton("Chaeldar");
	private final JComboBox monsterToCamp = new JComboBox();
	private final String pictureURL = "http://i.imgur.com/F3Om7gN.png";
	private FoodGui foodGui;
	private final JSpinner specialAttackPercentageSpinner = new JSpinner();
	private final JTextArea announcementTextArea = new JTextArea();
	private PotionGui potionGui;
	public final static DefaultListModel<String> consumablesListModel = new DefaultListModel<String>();
	public final static DefaultListModel<Integer> lootListModel = new DefaultListModel<Integer>();
	private final String fileName = "data/IkovAIOSlayer.ikovaioslayer";
	private final JCheckBox prayer = new JCheckBox("Prayer");
	private final JCheckBox protectionPrayers = new JCheckBox("Use Protection Prayers");
	private final JCheckBox looting = new JCheckBox("Custom Items");
	private JScrollPane consumablesScrollPane = new JScrollPane();
	private JScrollPane announcementAreaScrollPane = new JScrollPane();
	private final JMenuBar menuBar = new JMenuBar();
	private final JMenuItem saveMenuItem = new JMenuItem("Save Settings");
	private final JMenuItem loadMenuItem = new JMenuItem("Load Settings");
	private final JMenuItem parabotWebsiteMenuItem = new JMenuItem("Parabot Website");
	private final JMenuItem scriptThreadMenuItem = new JMenuItem("IkovAIOSlayer Thread");
	private final JMenuItem updateLogMenuItem = new JMenuItem("Update Log");
	private final JMenu fileMenu = new JMenu("File");
	private final JMenu helpMenu = new JMenu("Help");
	private final JPanel specialAttackPanel = new JPanel();
	private final JTextField primaryWeaponIdTextField = new JTextField();
	private final JButton startButton = new JButton("Start");
	private final JTextField specialAttackWeaponIdTextField = new JTextField();
	private final JCheckBox specialAttack = new JCheckBox("Special Attack");
	private final JButton ShowInventoryItemIdsButton = new JButton("Show Inventory Item IDs");
	private boolean loaded;
	private final DefaultListModel<String> prayersListModel = new DefaultListModel<String>();
	private final DefaultListModel<String> prayersToUseListModel = new DefaultListModel<String>();
	private final JList prayersToUseList = new JList(prayersToUseListModel);
	private final JScrollPane prayersToUseScrollPane = new JScrollPane();
	private final JButton addToPrayersButton = new JButton("-->");
	private final JButton takeFromPrayersButton = new JButton("<--");
	private final JList prayersList = new JList(prayersListModel);
	private final JScrollPane prayersScrollPane = new JScrollPane();
	private final JComboBox prayerFilter = new JComboBox();
	private final JList<Integer> lootList = new JList<Integer>(lootListModel);
	private final JButton addLootItemButton = new JButton("Add Item");
	private final JButton deleteSelectedLootItem = new JButton("Delete Selected Item");
	private final JButton clearLootItemList = new JButton("Clear List");

	/**
	 * Create the frame.
	 */
	public Gui() {
		setTitle("IkovAIOSlayer");
		setResizable(false);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 407, 325);

		setJMenuBar(menuBar);
		menuBar.add(fileMenu);
		menuBar.add(helpMenu);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		fileMenu.add(saveMenuItem);
		saveMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "Are you sure you want to override your last save?", "Warning!",
						JOptionPane.YES_NO_OPTION) == 0) {
					save();
				}
			}
		});
		fileMenu.add(loadMenuItem);
		loadMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				load();
			}
		});
		helpMenu.add(parabotWebsiteMenuItem);
		parabotWebsiteMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().browse(new URI("https://www.parabot.org/home/"));
				} catch (IOException | URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});
		helpMenu.add(scriptThreadMenuItem);
		scriptThreadMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().browse(new URI("https://www.parabot.org/community/topic/12543-ikov-aioslayer-free/"));
				} catch (IOException | URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});
		helpMenu.add(updateLogMenuItem);
		updateLogMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().browse(new URI("http://pastebin.com/vJntsWsM"));
				} catch (IOException | URISyntaxException e1) {
					e1.printStackTrace();
				}
			}
		});

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(0, 0, 405, 278);
		contentPane.add(tabbedPane);

		JPanel mainPanel = new JPanel();
		tabbedPane.addTab("Main", null, mainPanel, null);
		mainPanel.setLayout(null);

		try {
			URL url = new URL(pictureURL);
			BufferedImage image = ImageIO.read(url);
			JLabel eSlayer = new JLabel(new ImageIcon(image));
			eSlayer.setBackground(new Color(255, 255, 255));
			eSlayer.setBounds(6, 0, 384, 98);
			mainPanel.add(eSlayer);
		} catch (Exception e) {
			e.printStackTrace();
		}
		slayer.setToolTipText("Train slayer option.");

		slayer.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JRadioButton source = (JRadioButton) e.getSource();
				if (source.isSelected()) {
					mazchna.setEnabled(true);
					vannaka.setEnabled(true);
					duradel.setEnabled(true);
					chaeldar.setEnabled(true);
					// monsterToCamp.setEnabled(false);
				}
			}
		});
		slayer.setSelected(true);
		modeButtonGroup.add(slayer);
		slayer.setBounds(58, 96, 109, 23);
		mainPanel.add(slayer);
		campMonster.setToolTipText("Camp a selected monster option.");

		campMonster.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JRadioButton source = (JRadioButton) e.getSource();
				if (source.isSelected()) {
					mazchna.setEnabled(false);
					vannaka.setEnabled(false);
					duradel.setEnabled(false);
					chaeldar.setEnabled(false);
					// monsterToCamp.setEnabled(true);
				}
			}
		});
		modeButtonGroup.add(campMonster);
		campMonster.setBounds(270, 96, 109, 23);
		mainPanel.add(campMonster);
		mazchna.setToolTipText("Mazachna slayer master.");

		mazchna.setSelected(true);
		slayerMasterButtonGroup.add(mazchna);
		mazchna.setBounds(16, 122, 109, 23);
		mainPanel.add(mazchna);

		slayerMasterButtonGroup.add(vannaka);
		vannaka.setToolTipText("Vannaka slayer master.");
		vannaka.setBounds(139, 122, 109, 23);
		mainPanel.add(vannaka);

		slayerMasterButtonGroup.add(duradel);
		duradel.setToolTipText("Duradel slayer master.");
		duradel.setBounds(16, 195, 109, 23);
		mainPanel.add(duradel);

		slayerMasterButtonGroup.add(chaeldar);
		chaeldar.setToolTipText("Chaeldar slayer master.");
		chaeldar.setBounds(139, 195, 109, 23);
		mainPanel.add(chaeldar);
		monsterToCamp.setToolTipText("Monster to camp.");

		monsterToCamp.setEnabled(false);
		monsterToCamp.setModel(new DefaultComboBoxModel(new String[] { "Frost Dragon", "Blue Dragon", "Green Dragon",
				"Dark Beast" }));
		monsterToCamp.setBounds(270, 159, 120, 23);
		mainPanel.add(monsterToCamp);

		JLabel lblMonsterToCamp = new JLabel("Monster to Camp");
		lblMonsterToCamp.setBounds(285, 146, 87, 14);
		mainPanel.add(lblMonsterToCamp);

		JPanel consumablePanel = new JPanel();
		tabbedPane.addTab("Consumables", null, consumablePanel, null);
		consumablePanel.setLayout(null);

		JLabel lblConsumables = new JLabel("Consumables");
		lblConsumables.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblConsumables.setBounds(153, 11, 107, 14);
		consumablePanel.add(lblConsumables);

		JButton addFoodButton = new JButton("Add Food");
		addFoodButton.setToolTipText("Add food to consumables list.");
		addFoodButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (foodGui == null) {
					foodGui = new FoodGui();
				}
				if (!foodGui.isVisible()) {
					foodGui.setVisible(true);
				}
			}
		});
		addFoodButton.setBounds(28, 142, 89, 23);
		consumablePanel.add(addFoodButton);

		JButton addPotionButton = new JButton("Add Potion");
		addPotionButton.setToolTipText("Add potion to consumables list.");
		addPotionButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (potionGui == null) {
					potionGui = new PotionGui();
				}
				if (!potionGui.isVisible()) {
					potionGui.setVisible(true);
				}

			}
		});
		addPotionButton.setBounds(278, 142, 89, 23);
		consumablePanel.add(addPotionButton);

		JButton clearListButton = new JButton("Clear List");
		clearListButton.setToolTipText("Clear consumables list.");
		clearListButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the WHOLE list?", "Warning!",
						JOptionPane.YES_NO_OPTION) == 0) {
					consumablesListModel.clear();
				}
			}
		});
		clearListButton.setBounds(29, 192, 89, 23);
		consumablePanel.add(clearListButton);

		JButton deleteSelectedButton = new JButton("Delete Selected");
		deleteSelectedButton.setToolTipText("Delete selected consumable off consumables list.");
		deleteSelectedButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				consumablesListModel.removeElementAt(consumables.getSelectedIndex());
			}
		});
		deleteSelectedButton.setBounds(268, 192, 107, 23);
		consumablePanel.add(deleteSelectedButton);

		consumablesScrollPane.setBounds(48, 36, 301, 95);
		consumablePanel.add(consumablesScrollPane);
		consumables.setToolTipText("List of consumables.");

		consumablesScrollPane.setViewportView(consumables);
		consumables.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		JPanel prayerPanel = new JPanel();
		tabbedPane.addTab("Prayer", null, prayerPanel, null);
		prayerPanel.setLayout(null);

		protectionPrayers.setToolTipText("Use protectionPrayers in combat.");
		protectionPrayers.setEnabled(false);
		protectionPrayers.setBounds(204, 7, 138, 23);
		prayerPanel.add(protectionPrayers);
		prayer.setToolTipText("Use prayers in combat.");

		prayer.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JCheckBox source = (JCheckBox) e.getSource();
				if (source.isSelected()) {
					protectionPrayers.setEnabled(true);
					addToPrayersButton.setEnabled(true);
					takeFromPrayersButton.setEnabled(true);
					prayerFilter.setEnabled(true);
					loadPrayers(3);
					return;
				}
				prayerFilter.setEnabled(false);
				prayerFilter.setSelectedIndex(0);
				prayersToUseListModel.clear();
				prayersListModel.clear();
				takeFromPrayersButton.setEnabled(false);
				addToPrayersButton.setEnabled(false);
				protectionPrayers.setEnabled(false);
				protectionPrayers.setSelected(false);
			}
		});
		prayer.setBounds(105, 7, 97, 23);
		prayerPanel.add(prayer);

		prayersScrollPane.setBounds(10, 37, 138, 202);
		prayerPanel.add(prayersScrollPane);
		prayersList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		prayersScrollPane.setViewportView(prayersList);
		prayersToUseScrollPane.setBounds(248, 37, 142, 202);

		prayerPanel.add(prayersToUseScrollPane);
		prayersToUseList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		prayersToUseScrollPane.setViewportView(prayersToUseList);
		addToPrayersButton.setEnabled(false);
		addToPrayersButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!prayersList.isSelectionEmpty()) {
					prayersToUseListModel.addElement(prayersListModel.get(prayersList.getSelectedIndex()));
					prayersListModel.remove(prayersList.getSelectedIndex());
				}
			}
		});
		addToPrayersButton.setToolTipText("Add to prayers list.");
		addToPrayersButton.setBounds(158, 101, 80, 45);

		prayerPanel.add(addToPrayersButton);
		takeFromPrayersButton.setEnabled(false);
		takeFromPrayersButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!prayersToUseList.isSelectionEmpty()) {
					prayersListModel.addElement(prayersToUseListModel.get(prayersToUseList.getSelectedIndex()));
					prayersToUseListModel.remove(prayersToUseList.getSelectedIndex());
				}
			}
		});
		takeFromPrayersButton.setToolTipText("Take from prayers list.");
		takeFromPrayersButton.setBounds(158, 178, 80, 45);

		prayerPanel.add(takeFromPrayersButton);
		prayerFilter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				loadPrayers(prayerFilter.getSelectedIndex());
			}
		});

		prayerFilter.setEnabled(false);
		prayerFilter.setModel(new DefaultComboBoxModel(new String[] { "All", "Normal", "Curses" }));
		prayerFilter.setBounds(158, 51, 80, 20);
		prayerPanel.add(prayerFilter);

		JLabel lblFilter = new JLabel("Filter");
		lblFilter.setBounds(184, 37, 46, 14);
		prayerPanel.add(lblFilter);

		JPanel lootingPanel = new JPanel();
		tabbedPane.addTab("Looting", null, lootingPanel, null);
		lootingPanel.setLayout(null);
		looting.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JCheckBox source = (JCheckBox) e.getSource();
				if (source.isSelected()) {
					clearLootItemList.setEnabled(true);
					addLootItemButton.setEnabled(true);
					deleteSelectedLootItem.setEnabled(true);
				} else {
					lootListModel.clear();
					clearLootItemList.setEnabled(false);
					addLootItemButton.setEnabled(false);
					deleteSelectedLootItem.setEnabled(false);
				}
			}
		});
		looting.setToolTipText("Loot items on custom items list.");
		looting.setBounds(232, 33, 97, 23);
		lootingPanel.add(looting);

		lootList.setToolTipText("List of items to loot.");
		lootList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		lootList.setBounds(22, 36, 142, 203);
		lootingPanel.add(lootList);

		JLabel lblCustomLootItems = new JLabel("Custom Loot Items");
		lblCustomLootItems.setBounds(45, 11, 97, 14);
		lootingPanel.add(lblCustomLootItems);

		addLootItemButton.setToolTipText("Add item to custom loot item list.");
		addLootItemButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				lootListModel.addElement(Integer.parseInt(JOptionPane.showInputDialog(null, "Enter item ID.")));
			}
		});
		addLootItemButton.setBounds(178, 91, 89, 23);
		lootingPanel.add(addLootItemButton);

		deleteSelectedLootItem.setToolTipText("Delete selected item from loot item list.");
		deleteSelectedLootItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lootListModel.removeElementAt(lootList.getSelectedIndex());
			}
		});
		deleteSelectedLootItem.setBounds(205, 142, 142, 23);
		lootingPanel.add(deleteSelectedLootItem);

		clearLootItemList.setToolTipText("Clear custom loot item list.");
		clearLootItemList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JOptionPane.showConfirmDialog(null, "Are you sure you want to clear the WHOLE list?", "Warning!",
						JOptionPane.YES_NO_OPTION) == 0) {
					lootListModel.clear();
				}
			}
		});
		clearLootItemList.setBounds(279, 91, 89, 23);
		lootingPanel.add(clearLootItemList);

		tabbedPane.addTab("Special Attack", null, specialAttackPanel, null);
		specialAttackPanel.setLayout(null);
		specialAttack.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				JCheckBox source = (JCheckBox) e.getSource();
				if (source.isSelected()) {
					primaryWeaponIdTextField.setEnabled(true);
					specialAttackWeaponIdTextField.setEnabled(true);
					ShowInventoryItemIdsButton.setEnabled(true);
					specialAttackPercentageSpinner.setEnabled(true);
				} else {
					primaryWeaponIdTextField.setEnabled(false);
					primaryWeaponIdTextField.setText("");
					specialAttackWeaponIdTextField.setEnabled(false);
					specialAttackWeaponIdTextField.setText("");
					ShowInventoryItemIdsButton.setEnabled(false);
					Variables.setPaintIds(false);
					specialAttackPercentageSpinner.setEnabled(false);
					specialAttackPercentageSpinner.setValue(0);
				}

			}
		});

		specialAttack.setToolTipText("Use special attack.");
		specialAttack.setBounds(150, 18, 97, 23);
		specialAttackPanel.add(specialAttack);

		primaryWeaponIdTextField.setEnabled(false);
		primaryWeaponIdTextField.setToolTipText("Primary weapon ID.");
		primaryWeaponIdTextField.setBounds(39, 82, 119, 23);
		specialAttackPanel.add(primaryWeaponIdTextField);
		primaryWeaponIdTextField.setColumns(10);

		JLabel lblPrimaryWeapon = new JLabel("Primary Weapon");
		lblPrimaryWeapon.setBounds(60, 57, 85, 14);
		specialAttackPanel.add(lblPrimaryWeapon);

		specialAttackWeaponIdTextField.setEnabled(false);
		specialAttackWeaponIdTextField.setToolTipText("Special attack weapon ID.");
		specialAttackWeaponIdTextField.setColumns(10);
		specialAttackWeaponIdTextField.setBounds(221, 83, 119, 23);
		specialAttackPanel.add(specialAttackWeaponIdTextField);

		JLabel lblSpecialAttackWeapon = new JLabel("Special Attack Weapon");
		lblSpecialAttackWeapon.setBounds(221, 57, 119, 14);
		specialAttackPanel.add(lblSpecialAttackWeapon);

		ShowInventoryItemIdsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Variables.setPaintIds(Variables.isPaintIds() ? false : true);
			}
		});
		ShowInventoryItemIdsButton.setEnabled(false);
		ShowInventoryItemIdsButton.setToolTipText("Show inventory item ids.");
		ShowInventoryItemIdsButton.setBounds(107, 177, 173, 23);
		specialAttackPanel.add(ShowInventoryItemIdsButton);
		specialAttackPercentageSpinner.setEnabled(false);

		specialAttackPercentageSpinner.setToolTipText("Special attack % needed");
		specialAttackPercentageSpinner.setModel(new SpinnerNumberModel(0, 0, 100, 5));
		specialAttackPercentageSpinner.setBounds(173, 141, 36, 20);
		specialAttackPanel.add(specialAttackPercentageSpinner);

		JLabel lblToSpecial = new JLabel("Special attack % needed");
		lblToSpecial.setBounds(128, 116, 129, 14);
		specialAttackPanel.add(lblToSpecial);

		JPanel announcementPanel = new JPanel();
		tabbedPane.addTab("Announcements", null, announcementPanel, null);
		announcementPanel.setLayout(null);

		announcementAreaScrollPane.setBounds(29, 30, 344, 209);
		announcementPanel.add(announcementAreaScrollPane);
		announcementTextArea.setToolTipText("");

		announcementAreaScrollPane.setViewportView(announcementTextArea);
		announcementTextArea.setEditable(false);
		announcementTextArea.setLineWrap(true);
		Utils.loadInfo(announcementTextArea);

		JLabel lblAnnouncement = new JLabel("Announcements");
		lblAnnouncement.setBounds(170, 11, 97, 14);
		announcementPanel.add(lblAnnouncement);

		startButton.setToolTipText("Start script.");
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// handle consumables
				for (int i = 0; i < consumablesListModel.getSize(); i++) {
					if (consumablesListModel.getElementAt(i).toLowerCase().contains("food")) {
						for (Food food : Food.values()) {
							if (food.getName().equalsIgnoreCase(consumablesListModel.getElementAt(i).split(":")[1])) {
								Variables.addConsumable(food,
										new Integer[] { Integer.parseInt(consumablesListModel.getElementAt(i).split(":")[2]),
												Integer.parseInt(consumablesListModel.getElementAt(i).split(":")[3]) });
							}
						}
					} else if (consumablesListModel.getElementAt(i).toLowerCase().contains("potion")) {
						for (Potion potion : Potion.values()) {
							if (potion.getName().equalsIgnoreCase(consumablesListModel.getElementAt(i).split(":")[1])) {
								Variables.addConsumable(potion,
										new Integer[] { Integer.parseInt(consumablesListModel.getElementAt(i).split(":")[2]) });
							}
						}
					}
				}
				// handle special attack
				if (specialAttack.isSelected()) {
					Variables.setPrimaryWeapon(Integer.parseInt(primaryWeaponIdTextField.getText()));
					Variables.setSpecialAttack(true);
					Variables.setSpecialWeapon(Integer.parseInt(specialAttackWeaponIdTextField.getText()));
					Variables.setSpecialAttackAmountNeed(Integer.parseInt(specialAttackPercentageSpinner.getValue().toString()));
				}
				// handle mode and slayer master
				if (slayer.isSelected()) {
					if (mazchna.isSelected()) {
						Variables.setMaster(SlayerMaster.MAZCHNA);
					} else if (duradel.isSelected()) {
						Variables.setMaster(SlayerMaster.DURADEL);
					} else if (vannaka.isSelected()) {
						Variables.setMaster(SlayerMaster.VANNAKA);
					} else {
						Variables.setMaster(SlayerMaster.CHAELDAR);
					}
				} else {
					Variables.setCamping(true);
					Variables.setMonsterToCamp(monsterToCamp.getSelectedItem().toString().toLowerCase());

				}
				// handle prayers
				if (prayer.isSelected()) {
					Variables.setPray(true);
					if (!prayersToUseListModel.isEmpty()) {
						for (int i = 0; i < prayersToUseListModel.size(); i++) {
							for (Prayer prayer : Prayer.values()) {
								if (prayersToUseListModel.get(i).equalsIgnoreCase(prayer.toString())) {
									Variables.addPrayer(prayer);
								}
							}
						}
					}
					if (protectionPrayers.isSelected()) {
						Variables.setOverheads(true);
					}
				}
				// handle looting
				if (looting.isSelected() && !lootListModel.isEmpty()) {
					int[] s = new int[lootListModel.size()];
					for (int i = 0; i < s.length; i++) {
						s[i] = lootListModel.getElementAt(i);
					}
					Variables.addLoot(s);
				}
				Variables.setPaintIds(false);
				dispose();
			}
		});
		startButton.setBounds(283, 205, 89, 34);
		mainPanel.add(startButton);
	}

	private void loadPrayers(int type) {
		prayersListModel.clear();
		for (Prayer prayer : Prayer.values()) {
			if (!prayer.toString().toLowerCase().contains("deflect") && !prayer.toString().toLowerCase().contains("from")
					&& !prayersListModel.contains(prayer.toString()) && !prayersToUseListModel.contains(prayer.toString())) {
				switch (type) {
				case 1:
					if (prayer.isNormal()) {
						prayersListModel.addElement(prayer.toString());
					}
					break;
				case 2:
					if (!prayer.isNormal()) {
						prayersListModel.addElement(prayer.toString());
					}
					break;
				default:
					prayersListModel.addElement(prayer.toString());
					break;
				}
			}
		}
	}

	public static void addConsumable(String i) {
		for (int s = 0; s < consumablesListModel.getSize(); s++) {
			if (consumablesListModel.getElementAt(s).toLowerCase().contains("food") && i.toLowerCase().contains("food")) {
				JOptionPane.showMessageDialog(null,
						"A food type has already been added to your consumables list. Please remove it before adding a new one.");
				return;
			}
			if (consumablesListModel.getElementAt(s).contains(i.split(":")[1])) {
				JOptionPane.showMessageDialog(null,
						"This consumable has already been added. Please remove the previous one before adding a new one.");
				return;
			}
		}
		consumablesListModel.addElement(i);
	}

	private void save() {

		Properties prop = new Properties();
		OutputStream output = null;

		try {
			File file = new File(fileName);
			if (file.exists() && file.delete()) {
				file.getParentFile().mkdir();
				file.createNewFile();
			}
			output = new FileOutputStream(fileName);

			if (mazchna.isSelected()) {

				prop.setProperty("masterMazachna", "true");

			}

			if (vannaka.isSelected()) {

				prop.setProperty("masterVannaka", "true");

			}

			if (chaeldar.isSelected()) {

				prop.setProperty("masterChaelder", "true");

			}

			if (duradel.isSelected()) {

				prop.setProperty("masterDuradel", "true");

			}

			if (protectionPrayers.isSelected()) {
				prop.setProperty("protectionPrayers", "true");
			}
			if (prayer.isSelected()) {
				prop.setProperty("usePrayer", "true");
				if (!prayersToUseListModel.isEmpty()) {
					for (int i = 0; i < prayersToUseListModel.size(); i++) {
						prop.setProperty("prayer" + i, prayersToUseListModel.getElementAt(i));
					}
				}
			}

			if (looting.isSelected()) {
				prop.setProperty("looting", "true");
				for (int i = 0; i < lootListModel.getSize(); i++) {
					prop.setProperty("lootitem" + i, lootListModel.getElementAt(i).toString());
				}
			}
			if (campMonster.isSelected()) {
				prop.setProperty("camp", "true");
			}
			if (slayer.isSelected()) {
				prop.setProperty("slayer", "true");
			}
			if (specialAttack.isSelected()) {
				prop.setProperty("specialAttack", "true");
				prop.setProperty("primaryWeaponId", primaryWeaponIdTextField.getText());
				prop.setProperty("specialAttackWeaponId", specialAttackWeaponIdTextField.getText());
				prop.setProperty("specPercentage", specialAttackPercentageSpinner.getValue().toString());
			}
			for (int i = 0; i < consumablesListModel.getSize(); i++) {
				prop.setProperty("consumables" + i, consumablesListModel.getElementAt(i));
			}
			prop.setProperty("monsterToCamp", monsterToCamp.getSelectedItem().toString());
			// save properties to project root folder
			prop.store(output, null);
			JOptionPane.showMessageDialog(null, "Settings successfully saved.");
			loaded = false;
		} catch (IOException io) {
			JOptionPane.showMessageDialog(null, "Failed to save settings.");
			io.printStackTrace();
		}
	}

	private void load() {
		if (loaded) {
			JOptionPane.showMessageDialog(null, "Your settings are already loaded.");
			return;
		}
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream(fileName);
			// load a properties file
			prop.load(input);
			// get the property value and load it
			slayer.setSelected(Boolean.parseBoolean(prop.getProperty("slayer")));
			campMonster.setSelected(Boolean.parseBoolean(prop.getProperty("camp")));
			monsterToCamp.setSelectedItem(prop.getProperty("monsterToCamp"));
			mazchna.setSelected(Boolean.parseBoolean(prop.getProperty("masterMazachna")));
			vannaka.setSelected(Boolean.parseBoolean(prop.getProperty("masterVannaka")));
			chaeldar.setSelected(Boolean.parseBoolean(prop.getProperty("masterChaelder")));
			duradel.setSelected(Boolean.parseBoolean(prop.getProperty("masterDuradel")));
			prayer.setSelected(Boolean.parseBoolean(prop.getProperty("usePrayer")));
			protectionPrayers.setSelected(Boolean.parseBoolean(prop.getProperty("protectionPrayers")));
			looting.setSelected(Boolean.parseBoolean(prop.getProperty("looting")));
			if (Boolean.parseBoolean(prop.getProperty("specialAttack"))) {
				specialAttack.setSelected(Boolean.parseBoolean(prop.getProperty("specialAttack")));
				specialAttackWeaponIdTextField.setText(prop.getProperty("specialAttackWeaponId"));
				primaryWeaponIdTextField.setText(prop.getProperty("primaryWeaponId"));
				specialAttackPercentageSpinner.setValue(Integer.parseInt(prop.getProperty("specPercentage")));
			}
			consumablesListModel.clear();
			for (String i : prop.stringPropertyNames()) {
				if (i.contains("consumables")) {
					consumablesListModel.addElement(prop.getProperty(i));
				}
			}
			prayersToUseListModel.clear();
			for (String i : prop.stringPropertyNames()) {
				if (i.contains("prayer")) {
					prayersToUseListModel.addElement(prop.getProperty(i));
				}
			}
			lootListModel.clear();
			for (String i : prop.stringPropertyNames()) {
				if (i.contains("lootitem")) {
					lootListModel.addElement(Integer.parseInt(prop.getProperty(i)));
				}
			}
			JOptionPane.showMessageDialog(null, "Settings succesfully loaded.");
			loaded = true;
		} catch (IOException io) {
			JOptionPane.showMessageDialog(null, "Failed to load settings.");
			io.printStackTrace();

		}
	}
}
