package com.ikovps.slayer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;

import javax.imageio.ImageIO;

import org.parabot.core.ui.Logger;
import org.parabot.environment.api.interfaces.Paintable;
import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.Category;
import org.parabot.environment.scripts.Script;
import org.parabot.environment.scripts.ScriptManifest;
import org.rev317.min.api.events.MessageEvent;
import org.rev317.min.api.events.listeners.MessageListener;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.wrappers.Item;

import com.ikovps.slayer.data.Constants;
import com.ikovps.slayer.data.Monster;
import com.ikovps.slayer.data.Task;
import com.ikovps.slayer.data.Variables;
import com.ikovps.slayer.gui.Gui;
import com.ikovps.slayer.strategies.HandleBanking;
import com.ikovps.slayer.strategies.HandleCombat;
import com.ikovps.slayer.strategies.HandleConsumables;
import com.ikovps.slayer.strategies.HandleLogin;
import com.ikovps.slayer.strategies.HandleLooting;
import com.ikovps.slayer.strategies.HandlePrayer;
import com.ikovps.slayer.strategies.ManageInventory;
import com.ikovps.slayer.strategies.ObtainTask;
import com.ikovps.slayer.strategies.TravelToTask;
import com.ikovps.slayer.util.ScriptTimer;
import com.ikovps.slayer.util.Utils;

@ScriptManifest(author = "Empathy & El Maestro", category = Category.SLAYER, description = "An AIO slayer script for Ikov.", name = "IkovAIOSlayer", servers = { "Ikov" }, version = Constants.VERSION)
public class IkovAIOSlayer extends Script implements Paintable, MessageListener {

	private final Color color1 = new Color(153, 0, 0);
	private final Font font1 = new Font("Arial", 1, 22);
	private final Font font2 = new Font("Arial", 1, 16);
	private final Font font3 = new Font("Arial", 1, 13);
	private final Image img1 = getImage("http://i.imgur.com/KwX69Vi.png");
	private ScriptTimer slayerTimer;
	private int slayerXp;

	@Override
	public boolean onExecute() {
		slayerTimer = new ScriptTimer(Skill.SLAYER);
		slayerTimer.getXpGained();
		Gui gui = new Gui();
		gui.setVisible(true);
		while (gui.isVisible()) {
			Time.sleep(1000);
		}
		provide(Arrays.asList(new HandleLogin(), new ManageInventory(), new HandleConsumables(), new HandlePrayer(),
				new HandleLooting(), new ObtainTask(), new HandleBanking(), new TravelToTask(), new HandleCombat()));
		Variables.setShowPaint(true);
		return true;
	}

	public void onFinish() {
		Logger.addMessage("XP Gained: " + slayerTimer.getXpGained() + ", Tasks Completed: " + Variables.getTasksCompleted()
				+ ", Time Ran: " + slayerTimer.toString(), true);
	}

	private Image getImage(String url) {
		try {
			return ImageIO.read(new URL(url));
		} catch (IOException e) {
			return null;
		}
	}

	@Override
	public void paint(Graphics g1) {
		Graphics2D g = (Graphics2D) g1;
		if (Variables.isPaintIds()) {
			g.setColor(Color.green);
			for (Item i : Inventory.getItems()) {
				if (i != null) {
					int xOffset = ((i.getSlot() % 4) * 42) + 583;
					int yOffset = ((i.getSlot() / 4) * 36) + 216;
					g.drawString(Integer.toString(i.getId()), xOffset, yOffset);
				}
			}
		}
		if (slayerXp != slayerTimer.getXpGained()) {
			slayerXp = slayerTimer.getXpGained();
			Variables.getTask().setKillsLeft(Variables.getTask().getKillsLeft() - 1);
			Variables.setMonstersKilled(Variables.getMonstersKilled() + 1);
		}
		if (Variables.isShowPaint()) {
			g.drawImage(img1, 2, 1, null);
			g.setFont(font1);
			g.setColor(color1);
			g.drawString(Variables.getStatus(), 75, 495);
			g.drawString(slayerTimer.toString(), 109, 373);
			g.drawString("" + slayerTimer.getCurrentRealLevel(), 235, 404);
			g.drawString("" + slayerTimer.levelsGained(), 231, 436);
			g.drawString("" + Variables.getPointsEarned() + "(" + slayerTimer.getPerHour(Variables.getPointsEarned()) + ")", 202,
					469);
			g.setFont(font2);
			g.drawString((Variables.getTask() != null && !Variables.getTask().isDone()) ? (Variables.getTask().getKillsLeft()
					+ " " + Variables.getTask().getMonsterName()) : "Need a task...", 563, 257);
			g.drawString("" + Variables.getTasksCompleted() + "(" + slayerTimer.getPerHour(Variables.getTasksCompleted()) + ")",
					563, 306);
			g.drawString(
					"" + Utils.formatNumber(slayerTimer.getXpGained()) + "("
							+ Utils.formatNumber(slayerTimer.getPerHour(slayerTimer.getXpGained())) + ")", 563, 352);
			g.drawString("" + Variables.getMonstersKilled() + "(" + slayerTimer.getPerHour(Variables.getMonstersKilled()) + ")",
					563, 399);
			g.drawString((slayerTimer.getXpGained() <= 20) ? "Not sure yet..." : "" + slayerTimer.timeTillLevel(), 563, 447);
			g.setFont(font3);
			g.drawString("" + Constants.VERSION, 589, 466);
		}
	}

	@Override
	public void messageReceived(MessageEvent m) {
		if (m.getType() == 0) {
			if ((m.getMessage().contains("have completed your task"))) {
				Variables.setTasksCompleted(Variables.getTasksCompleted() + 1);
				Variables.getTask().setKillsLeft(0);
				Variables.setPointsEarned(Variables.getPointsEarned()
						+ Integer.parseInt(m.getMessage().split("recieved ")[1].split(" slayer")[0]));
			} else if (m.getMessage().contains("That command does not exist")) {
				Variables.setShowPaint(Variables.isShowPaint() ? false : true);
			} else if (m.getMessage().contains("That object does not exist")) {
				Variables.setStatus("Our player is bugged...Relogging...", true);
				Utils.dropClient();
			}
		}
	}
}