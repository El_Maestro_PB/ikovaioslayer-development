package com.ikovps.slayer.data;

import org.rev317.min.api.methods.Calculations;
import org.rev317.min.api.methods.Npcs;

import com.ikovps.slayer.util.CustomPath;

public class Task {

	private Monster monster;
	private int killsLeft;

	public Task(Monster monster, int killsLeft) {
		this.monster = monster;
		this.killsLeft = killsLeft;
	}

	public CustomPath getPath() {
		return monster.getPath();
	}

	public int getKillsLeft() {
		return killsLeft;
	}

	public void setKillsLeft(int i) {
		killsLeft = i;
	}

	public Monster getMonster() {
		return monster;
	}

	public String getMonsterName() {
		return monster.getName();
	}

	public int[] getNpcIds() {
		return monster.getNpcIds();
	}

	public String getPrayerType() {
		return monster.getPrayerType();
	}

	public boolean requiresDragonShield() {
		return monster.getName().toLowerCase().contains("dragon");
	}

	public void travelTo() {
		if (atTask()) {
			return;
		}
		if (monster.getPath().isTraversable()) {
			Variables.setStatus("Traveling to " + Variables.getTask().getMonsterName() + "...", true);
			monster.getPath().traverse();
		}
	}

	public boolean atTask() {
		try {
			if (Npcs.getNearest(monster.getNpcIds()).length > 0 && Npcs.getNearest(monster.getNpcIds()) != null) {
				return Calculations.distanceBetween(Npcs.getNearest(monster.getNpcIds())[0].getLocation(), monster.getPath()
						.getLastTile()) <= 10
						&& monster.getPath().getLastTile().distanceTo() <= 6
						|| Npcs.getNearest(monster.getNpcIds())[0].distanceTo() <= 12;
			}
		} catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
		}
		return false;
	}

	public String toString() {
		return monster.getName() + ", kills left: " + killsLeft;
	}

	public boolean isDone() {
		return killsLeft <= 0;
	}

}