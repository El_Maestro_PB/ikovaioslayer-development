package com.ikovps.slayer.data;

import org.rev317.min.api.wrappers.Tile;

import com.ikovps.slayer.util.CustomPath;
import com.ikovps.slayer.util.Teleports;

/**
 * An enum containing all slayer tasks.
 * 
 * @author Masood && El Maestro
 *
 */
public enum Monster {

	ABERRANT_SPECTERS("Aberrant Specters", 1, new int[] { 1604 }, new CustomPath(Teleports.SLAYER_TOWER, new Tile(3426, 3537, 0),
			new Tile(3422, 3537, 0), new Tile(3418, 3537, 0), new Tile(3414, 3539, 0), new Tile(3412, 3543, 0), new Tile(3412,
					3547, 0), new Tile(3417, 3550, 0), new Tile(3419, 3550, 0), new Tile(3421, 3549, 0), 9319, new Tile(3425,
					3552, 1), new Tile(3419, 3552, 1), new Tile(3419, 3552, 1), new Tile(3419, 3548, 1), new Tile(3419, 3544, 1),
			new Tile(3423, 3540, 1)), "Melee"),

	BANSHEES("Banshees", 15, new int[] { 1612 }, new CustomPath(Teleports.SLAYER_TOWER, new Tile(3425, 3537, 0), new Tile(3421,
			3537, 0), new Tile(3418, 3537, 0), new Tile(3415, 3538, 0), new Tile(3412, 3542, 0), new Tile(3413, 3545, 0),
			new Tile(3417, 3545, 0), new Tile(3421, 3545, 0), new Tile(3424, 3545, 0), new Tile(3427, 3545, 0), new Tile(3427,
					3548, 0), new Tile(3427, 3552, 0), new Tile(3424, 3553, 0), new Tile(3420, 3557, 0), new Tile(3416, 3561, 0),
			new Tile(3415, 3563, 0), new Tile(3412, 3566, 0), new Tile(3415, 3568, 0), new Tile(3418, 3568, 0), new Tile(3423,
					3572, 0), new Tile(3427, 3573, 0), new Tile(3431, 3573, 0), new Tile(3435, 3572, 0), new Tile(3438, 3572, 0),
			new Tile(3443, 3572, 0), new Tile(3446, 3569, 0), new Tile(3446, 3565, 0), new Tile(3446, 3561, 0), new Tile(3442,
					3560, 0), new Tile(3439, 3562, 0)), "None"),

	BASILISKS("Basilisks", 40, new int[] { 1616 }, new CustomPath(Teleports.FREMENNIK_SLAYER_DUNGEON, 4499, new Tile(2804, 10001,
			0), new Tile(2801, 10001, 0), new Tile(2797, 9999, 0), new Tile(2793, 9997, 0), new Tile(2790, 9997, 0), new Tile(
			2787, 9996, 0), new Tile(2784, 9996, 0), new Tile(2780, 10000, 0), new Tile(2778, 10004, 0),
			new Tile(2777, 10009, 0), new Tile(2778, 10013, 0), new Tile(2783, 10016, 0), new Tile(2787, 10017, 0), new Tile(
					2791, 10018, 0), new Tile(2795, 10018, 0), new Tile(2799, 10020, 0), new Tile(2802, 10025, 0), new Tile(2799,
					10029, 0), new Tile(2796, 10032, 0), new Tile(2792, 10033, 0), new Tile(2786, 10033, 0), new Tile(2782,
					10034, 0), new Tile(2778, 10034, 0), new Tile(2774, 10032, 0), new Tile(2769, 10034, 0), new Tile(2765,
					10036, 0), new Tile(2761, 10034, 0), new Tile(2759, 10030, 0), new Tile(2759, 10027, 0), new Tile(2760,
					10023, 0), new Tile(2764, 10019, 0), new Tile(2764, 10015, 0), new Tile(2763, 10011, 0), new Tile(2761,
					10008, 0), new Tile(2759, 10004, 0), new Tile(2759, 9999, 0), new Tile(2754, 9996, 0),
			new Tile(2750, 9996, 0), new Tile(2746, 9999, 0), new Tile(2746, 10002, 0), new Tile(2746, 10007, 0)), "Melee"),

	BLACK_DEMONS("Black Demons", 1, new int[] { 84 }, new CustomPath(Teleports.BRIMHAVEN_DUNGEON, 5083, new Tile(2709, 9564, 0),
			new Tile(2705, 9564, 0), new Tile(2702, 9564, 0), new Tile(2698, 9564, 0), new Tile(2695, 9564, 0), new Tile(2691,
					9564, 0), 5103, new Tile(2687, 9564, 0), new Tile(2683, 9564, 0), new Tile(2680, 9564, 0), new Tile(2676,
					9564, 0), new Tile(2674, 9568, 0), new Tile(2672, 9572, 0), new Tile(2667, 9572, 0), new Tile(2664, 9570, 0),
			new Tile(2660, 9568, 0), new Tile(2656, 9566, 0), new Tile(2652, 9564, 0), new Tile(2650, 9562, 0), 5110, new Tile(
					2647, 9556, 0), new Tile(2647, 9551, 0), new Tile(2647, 9547, 0), new Tile(2644, 9542, 0), new Tile(2644,
					9537, 0), new Tile(2644, 9533, 0), new Tile(2642, 9529, 0), new Tile(2641, 9524, 0), new Tile(2641, 9520, 0),
			new Tile(2643, 9516, 0), new Tile(2646, 9512, 0), new Tile(2650, 9507, 0), new Tile(2653, 9504, 0), new Tile(2656,
					9500, 0), new Tile(2656, 9494, 0), new Tile(2654, 9489, 0), new Tile(2655, 9483, 0), new Tile(2660, 9483, 0),
			new Tile(2664, 9483, 0), new Tile(2669, 9481, 0), new Tile(2672, 9480, 0), 5106, new Tile(2680, 9478, 0), new Tile(
					2684, 9477, 0), new Tile(2688, 9477, 0), new Tile(2691, 9480, 0), new Tile(2693, 9482, 0), 5107, new Tile(
					2698, 9482, 0), new Tile(2702, 9482, 0), new Tile(2705, 9482, 0), new Tile(2709, 9484, 0)), "Melee"),

	BLOODVELDS("Bloodvelds", 1, new int[] { 1618 }, new CustomPath(Teleports.SLAYER_TOWER, new Tile(3425, 3537, 0), new Tile(
			3421, 3537, 0), new Tile(3418, 3537, 0), new Tile(3415, 3538, 0), new Tile(3412, 3542, 0), new Tile(3413, 3545, 0),
			new Tile(3417, 3545, 0), new Tile(3421, 3545, 0), new Tile(3424, 3545, 0), new Tile(3427, 3545, 0), new Tile(3427,
					3548, 0), new Tile(3427, 3552, 0), new Tile(3424, 3553, 0), new Tile(3420, 3557, 0), new Tile(3416, 3561, 0),
			new Tile(3415, 3563, 0), new Tile(3412, 3566, 0), new Tile(3415, 3568, 0), new Tile(3418, 3568, 0), new Tile(3423,
					3572, 0), new Tile(3427, 3573, 0), new Tile(3431, 3573, 0), new Tile(3435, 3572, 0), new Tile(3438, 3572, 0),
			new Tile(3443, 3572, 0), new Tile(3446, 3569, 0), new Tile(3446, 3565, 0), new Tile(3446, 3561, 0), new Tile(3442,
					3560, 0), new Tile(3439, 3559, 0), new Tile(3436, 3555, 0), new Tile(3436, 3551, 0), new Tile(3436, 3548, 0),
			new Tile(3437, 3546, 0), new Tile(3438, 3541, 0), new Tile(3438, 3538, 0), 4493, new Tile(3434, 3535, 1), new Tile(
					3438, 3535, 1), new Tile(3441, 3535, 1), new Tile(3445, 3536, 1), new Tile(3447, 3540, 1), new Tile(3447,
					3543, 1), new Tile(3447, 3546, 1), new Tile(3447, 3549, 1), new Tile(3447, 3550, 1), new Tile(3445, 3554, 1),
			new Tile(3441, 3556, 1), new Tile(3437, 3559, 1), new Tile(3437, 3561, 1), new Tile(3437, 3565, 1), new Tile(3434,
					3569, 1), new Tile(3429, 3573, 1), new Tile(3424, 3573, 1), new Tile(3421, 3573, 1), new Tile(3417, 3573, 1),
			new Tile(3413, 3570, 1), new Tile(3413, 3566, 1), new Tile(3413, 3562, 1), new Tile(3417, 3562, 1), new Tile(3420,
					3562, 1)), "Melee"),

	BRONZE_DRAGONS("Bronze Dragons", 1, new int[] { 1590 }, new CustomPath(Teleports.BRIMHAVEN_DUNGEON, 5083, new Tile(2709,
			9564, 0), new Tile(2705, 9564, 0), new Tile(2702, 9564, 0), new Tile(2698, 9564, 0), new Tile(2695, 9564, 0),
			new Tile(2691, 9564, 0), 5103, new Tile(2687, 9564, 0), new Tile(2683, 9564, 0), new Tile(2680, 9564, 0), new Tile(
					2676, 9564, 0), new Tile(2674, 9568, 0), new Tile(2672, 9572, 0), new Tile(2667, 9572, 0), new Tile(2664,
					9570, 0), new Tile(2660, 9568, 0), new Tile(2656, 9566, 0), new Tile(2652, 9564, 0), new Tile(2650, 9562, 0),
			5110, new Tile(2647, 9556, 0), new Tile(2647, 9551, 0), new Tile(2647, 9547, 0), new Tile(2644, 9542, 0), new Tile(
					2644, 9537, 0), new Tile(2644, 9533, 0), new Tile(2642, 9529, 0), new Tile(2641, 9524, 0), new Tile(2641,
					9520, 0), new Tile(2643, 9516, 0), new Tile(2646, 9512, 0), new Tile(2650, 9507, 0), new Tile(2653, 9504, 0),
			new Tile(2656, 9500, 0), new Tile(2656, 9494, 0), new Tile(2654, 9489, 0), new Tile(2655, 9483, 0), new Tile(2660,
					9483, 0), new Tile(2664, 9483, 0), new Tile(2669, 9481, 0), new Tile(2672, 9480, 0), 5106, new Tile(2680,
					9478, 0), new Tile(2684, 9477, 0), new Tile(2688, 9477, 0), new Tile(2691, 9480, 0), new Tile(2693, 9482, 0),
			5107, new Tile(2698, 9482, 0), new Tile(2702, 9482, 0), new Tile(2707, 9482, 0), new Tile(2711, 9482, 0), new Tile(
					2715, 9483, 0), new Tile(2720, 9483, 0), new Tile(2724, 9485, 0), new Tile(2728, 9485, 0)), "Melee"),

	CAVE_CRAWLERS("Cave Crawlers", 1, new int[] { 1600 }, new CustomPath(Teleports.FREMENNIK_SLAYER_DUNGEON, 4499, new Tile(2804,
			10001, 0), new Tile(2801, 10001, 0), new Tile(2797, 9999, 0), new Tile(2793, 9997, 0), new Tile(2790, 9997, 0)),
			"None"),

	CHAOS_DWARFS("Chaos Dwarfs", 1, new int[] { 119 }, new CustomPath(Teleports.TAVERLEY_DUNGEON, 1759, new Tile(2884, 9800, 0),
			new Tile(2884, 9804, 0), new Tile(2884, 9810, 0), new Tile(2884, 9813, 0), new Tile(2884, 9816, 0), new Tile(2884,
					9819, 0), new Tile(2884, 9824, 0), new Tile(2885, 9828, 0), new Tile(2885, 9831, 0), new Tile(2885, 9834, 0),
			new Tile(2885, 9838, 0), new Tile(2884, 9842, 0), new Tile(2887, 9843, 0), new Tile(2889, 9845, 0), new Tile(2892,
					9848, 0), new Tile(2896, 9848, 0), new Tile(2900, 9848, 0), new Tile(2903, 9848, 0), new Tile(2907, 9848, 0),
			new Tile(2909, 9849, 0), new Tile(2913, 9849, 0), new Tile(2918, 9849, 0), new Tile(2923, 9846, 0), new Tile(2926,
					9843, 0), new Tile(2929, 9841, 0), new Tile(2932, 9838, 0), new Tile(2935, 9834, 0), new Tile(2937, 9830, 0),
			new Tile(2937, 9827, 0), new Tile(2937, 9825, 0), new Tile(2937, 9820, 0), new Tile(2937, 9817, 0), new Tile(2937,
					9814, 0), new Tile(2938, 9810, 0), new Tile(2939, 9807, 0), new Tile(2941, 9803, 0), new Tile(2944, 9799, 0),
			new Tile(2946, 9798, 0), new Tile(2948, 9796, 0), new Tile(2951, 9793, 0), new Tile(2951, 9789, 0), new Tile(2951,
					9784, 0), new Tile(2951, 9781, 0), new Tile(2952, 9779, 0), new Tile(2950, 9775, 0), new Tile(2946, 9774, 0),
			new Tile(2941, 9777, 0), new Tile(2938, 9778, 0), new Tile(2935, 9774, 0), new Tile(2935, 9771, 0), new Tile(2935,
					9768, 0), new Tile(2935, 9764, 0), new Tile(2936, 9759, 0), new Tile(2934, 9756, 0), new Tile(2928, 9755, 0),
			new Tile(2923, 9755, 0), new Tile(2922, 9759, 0)), "None"),

	COCKATRICES("Cockatrices", 25, new int[] { 1620 }, new CustomPath(Teleports.FREMENNIK_SLAYER_DUNGEON, 4499, new Tile(2804,
			10001, 0), new Tile(2801, 10001, 0), new Tile(2797, 9999, 0), new Tile(2793, 9997, 0), new Tile(2790, 9997, 0),
			new Tile(2787, 9996, 0), new Tile(2784, 9996, 0), new Tile(2780, 10000, 0), new Tile(2778, 10004, 0), new Tile(2777,
					10009, 0), new Tile(2778, 10013, 0), new Tile(2783, 10016, 0), new Tile(2787, 10017, 0), new Tile(2791,
					10018, 0), new Tile(2795, 10018, 0), new Tile(2799, 10020, 0), new Tile(2802, 10025, 0), new Tile(2799,
					10029, 0), new Tile(2796, 10032, 0)), "None"),

	CRAWLING_HANDS("Crawling hands", 5, new int[] { 1648 }, new CustomPath(Teleports.SLAYER_TOWER, new Tile(3425, 3537, 0),
			new Tile(3422, 3537, 0), new Tile(3419, 3537, 0), new Tile(3415, 3537, 0), new Tile(3413, 3538, 0)), "None"),

	DAGANNOTHS("Dagannoths", 1, new int[] { 1338, 1342 }, new CustomPath(Teleports.CHAOS_TUNNELS, new Tile(3149, 5559, 0),
			new Tile(3147, 5556, 0), new Tile(3146, 5551, 0), new Tile(3148, 5547, 0)), "Range"),

	DUSTDEVILS("Dustdevils", 50, new int[] { 1624 }, new CustomPath(Teleports.CHAOS_TUNNELS, new Tile(3149, 5558, 0), new Tile(
			3147, 5555, 0), new Tile(3146, 5551, 0), new Tile(3146, 5547, 0), new Tile(3146, 5543, 0), 28779, new Tile(3146,
			5533, 0), new Tile(3149, 5529, 0), new Tile(3149, 5525, 0), new Tile(3149, 5520, 0), new Tile(3152, 5521, 0), 28779,
			new Tile(3158, 5523, 0), new Tile(3161, 5523, 0)), "Melee"),

	FIRE_GIANTS("Fire Giants", 1, new int[] { 110 }, new CustomPath(Teleports.BRIMHAVEN_DUNGEON, 5083, new Tile(2709, 9564, 0),
			new Tile(2705, 9564, 0), new Tile(2702, 9564, 0), new Tile(2698, 9564, 0), new Tile(2695, 9564, 0), new Tile(2691,
					9564, 0), 5103, new Tile(2687, 9564, 0), new Tile(2683, 9564, 0), new Tile(2680, 9564, 0), new Tile(2676,
					9564, 0), new Tile(2674, 9568, 0), new Tile(2672, 9572, 0), new Tile(2667, 9572, 0), new Tile(2664, 9570, 0),
			new Tile(2660, 9568, 0), new Tile(2656, 9566, 0), new Tile(2652, 9564, 0), new Tile(2650, 9562, 0), 5110, new Tile(
					2647, 9551, 0), new Tile(2647, 9547, 0), new Tile(2644, 9542, 0), new Tile(2644, 9537, 0), new Tile(2644,
					9533, 0), new Tile(2642, 9529, 0), new Tile(2641, 9524, 0), new Tile(2641, 9520, 0), new Tile(2643, 9516, 0),
			new Tile(2646, 9512, 0), new Tile(2650, 9507, 0), new Tile(2653, 9504, 0), new Tile(2656, 9500, 0), new Tile(2656,
					9494, 0), new Tile(2654, 9489, 0), new Tile(2655, 9483, 0)), "Melee"),

	GHOULS("Ghouls", 1, new int[] { 1218 }, new CustomPath(Teleports.GHOULS, new Tile(3416, 3510, 0)), "None"),

	GIANT_BATS("Giant Bats", 1, new int[] { 78 }, new CustomPath(Teleports.TAVERLEY_DUNGEON, 1759, new Tile(2884, 9802, 0),
			new Tile(2884, 9803, 0), new Tile(2884, 9806, 0), new Tile(2884, 9810, 0), new Tile(2884, 9812, 0), new Tile(2885,
					9815, 0), new Tile(2885, 9817, 0), new Tile(2885, 9821, 0), new Tile(2885, 9824, 0), new Tile(2885, 9828, 0),
			new Tile(2885, 9830, 0), new Tile(2885, 9834, 0), new Tile(2885, 9838, 0), new Tile(2884, 9841, 0), new Tile(2888,
					9844, 0), new Tile(2891, 9847, 0), new Tile(2893, 9848, 0), new Tile(2897, 9848, 0), new Tile(2900, 9848, 0),
			new Tile(2902, 9849, 0), new Tile(2904, 9849, 0), new Tile(2907, 9849, 0), new Tile(2910, 9849, 0), new Tile(2914,
					9849, 0), new Tile(2916, 9845, 0), new Tile(2916, 9843, 0), new Tile(2916, 9839, 0), new Tile(2915, 9837, 0),
			new Tile(2912, 9834, 0)), "None"),

	GREEN_DRAGONS("Green dragons", 23, new int[] { 941 }, new CustomPath(Teleports.CHAOS_TUNNELS, new Tile(3149, 5558, 0),
			new Tile(3147, 5555, 0), new Tile(3146, 5551, 0), new Tile(3146, 5547, 0), new Tile(3146, 5543, 0), 28779, new Tile(
					3146, 5533, 0), new Tile(3149, 5530, 0), new Tile(3149, 5526, 0), new Tile(3149, 5522, 0), new Tile(3149,
					5518, 0), new Tile(3147, 5515, 0)), "Melee"),

	HARPIE_BUG_SWARMS("Harpie bug swarms", 30, new int[] { 3153 }, new CustomPath(Teleports.BRIMHAVEN_DUNGEON, new Tile(2746,
			3150, 0), new Tile(2751, 3149, 0), new Tile(2757, 3147, 0), new Tile(2761, 3144, 0), new Tile(2766, 3144, 0),
			new Tile(2769, 3144, 0), new Tile(2771, 3140, 0), new Tile(2774, 3137, 0), new Tile(2777, 3134, 0), new Tile(2779,
					3134, 0), new Tile(2783, 3132, 0), new Tile(2786, 3129, 0), new Tile(2790, 3125, 0), new Tile(2792, 3123, 0),
			new Tile(2795, 3123, 0), new Tile(2799, 3123, 0), new Tile(2803, 3123, 0), new Tile(2807, 3122, 0), new Tile(2810,
					3122, 0), new Tile(2814, 3119, 0), new Tile(2818, 3115, 0), new Tile(2821, 3112, 0), new Tile(2826, 3111, 0),
			new Tile(2831, 3111, 0), new Tile(2835, 3111, 0), new Tile(2839, 3111, 0), new Tile(2843, 3111, 0), new Tile(2847,
					3111, 0), new Tile(2852, 3109, 0), new Tile(2856, 3107, 0), new Tile(2860, 3107, 0), new Tile(2864, 3107, 0),
			new Tile(2867, 3110, 0)), "None"),

	HILL_GIANTS("Hill Giants", 1, new int[] { 117 }, new CustomPath(Teleports.TAVERLEY_DUNGEON, 1759, new Tile(2884, 9800, 0),
			new Tile(2884, 9804, 0), new Tile(2884, 9810, 0), new Tile(2884, 9813, 0), new Tile(2884, 9816, 0), new Tile(2884,
					9819, 0), new Tile(2884, 9824, 0), new Tile(2885, 9828, 0), new Tile(2885, 9831, 0), new Tile(2885, 9834, 0),
			new Tile(2885, 9838, 0), new Tile(2884, 9842, 0), new Tile(2887, 9843, 0), new Tile(2889, 9845, 0), new Tile(2892,
					9848, 0), new Tile(2896, 9848, 0), new Tile(2900, 9848, 0), new Tile(2903, 9848, 0), new Tile(2907, 9848, 0),
			new Tile(2909, 9849, 0), new Tile(2913, 9849, 0), new Tile(2918, 9849, 0), new Tile(2923, 9846, 0), new Tile(2926,
					9843, 0), new Tile(2929, 9841, 0), new Tile(2932, 9838, 0), new Tile(2935, 9834, 0), new Tile(2937, 9830, 0),
			new Tile(2937, 9827, 0), new Tile(2937, 9825, 0), new Tile(2937, 9820, 0), new Tile(2937, 9817, 0), new Tile(2937,
					9814, 0), new Tile(2938, 9810, 0), new Tile(2939, 9807, 0), new Tile(2941, 9803, 0), new Tile(2944, 9799, 0),
			new Tile(2946, 9798, 0), new Tile(2948, 9796, 0), new Tile(2951, 9793, 0), new Tile(2951, 9789, 0), new Tile(2951,
					9784, 0), new Tile(2951, 9781, 0), new Tile(2952, 9779, 0), new Tile(2950, 9775, 0), new Tile(2946, 9774, 0),
			new Tile(2941, 9777, 0), new Tile(2938, 9778, 0), new Tile(2935, 9774, 0), new Tile(2935, 9771, 0), new Tile(2935,
					9768, 0), new Tile(2935, 9764, 0), new Tile(2936, 9759, 0), new Tile(2934, 9756, 0), new Tile(2931, 9753, 0),
			new Tile(2931, 9755, 0), new Tile(2926, 9750, 0), new Tile(2924, 9749, 0), new Tile(2922, 9746, 0), new Tile(2919,
					9743, 0), new Tile(2915, 9742, 0), new Tile(2912, 9738, 0), new Tile(2912, 9736, 0), new Tile(2908, 9732, 0),
			new Tile(2905, 9731, 0)), "None"),

	HOBGOBLINS("Hobgoblins", 1, new int[] { 122 }, new CustomPath(Teleports.ASGARNIAN_ICE_DUNGEON, new Tile(3044, 9582, 0),
			new Tile(3040, 9582, 0), new Tile(3038, 9581, 0), new Tile(3034, 9581, 0), new Tile(3031, 9581, 0), new Tile(3026,
					9582, 0), new Tile(3022, 9582, 0), new Tile(3019, 9582, 0), new Tile(3016, 9580, 0), new Tile(3012, 9579, 0),
			new Tile(3007, 9579, 0)), "None"),

	ICE_GIANTS("Ice giants", 1, new int[] { 111 }, new CustomPath(Teleports.ASGARNIAN_ICE_DUNGEON, new Tile(3051, 9582, 0)),
			"Melee"),

	ICE_WARRIORS("Ice warriors", 1, new int[] { 125 }, new CustomPath(Teleports.ASGARNIAN_ICE_DUNGEON, new Tile(3051, 9582, 0)),
			"Melee"),

	INFERNAL_MAGES("Infernal Mages", 15, new int[] { 1643 }, new CustomPath(Teleports.SLAYER_TOWER, new Tile(3425, 3537, 0),
			new Tile(3421, 3537, 0), new Tile(3418, 3537, 0), new Tile(3415, 3538, 0), new Tile(3412, 3542, 0), new Tile(3413,
					3545, 0), new Tile(3417, 3545, 0), new Tile(3421, 3545, 0), new Tile(3424, 3545, 0), new Tile(3427, 3545, 0),
			new Tile(3427, 3548, 0), new Tile(3427, 3552, 0), new Tile(3424, 3553, 0), new Tile(3420, 3557, 0), new Tile(3416,
					3561, 0), new Tile(3415, 3563, 0), new Tile(3412, 3566, 0), new Tile(3415, 3568, 0), new Tile(3418, 3568, 0),
			new Tile(3423, 3572, 0), new Tile(3427, 3573, 0), new Tile(3431, 3573, 0), new Tile(3435, 3572, 0), new Tile(3438,
					3572, 0), new Tile(3443, 3572, 0), new Tile(3446, 3569, 0), new Tile(3446, 3565, 0), new Tile(3446, 3561, 0),
			new Tile(3442, 3560, 0), new Tile(3439, 3559, 0), new Tile(3436, 3555, 0), new Tile(3436, 3551, 0), new Tile(3436,
					3548, 0), new Tile(3437, 3546, 0), new Tile(3438, 3541, 0), new Tile(3438, 3538, 0), 4493, new Tile(3432,
					3539, 1), new Tile(3435, 3540, 1), new Tile(3439, 3540, 1), new Tile(3444, 3537, 1), new Tile(3447, 3539, 1),
			new Tile(3447, 3542, 1), new Tile(3447, 3546, 1), new Tile(3447, 3549, 1), new Tile(3446, 3552, 1), new Tile(3446,
					3554, 1), new Tile(3443, 3556, 1), new Tile(3440, 3557, 1), new Tile(3437, 3560, 1)), "Magic"),

	IRON_DRAGONS("Iron Dragons", 1, new int[] { 1591 },
			new CustomPath(Teleports.BRIMHAVEN_DUNGEON, 5083, new Tile(2709, 9564, 0), new Tile(2705, 9564, 0), new Tile(2702,
					9564, 0), new Tile(2698, 9564, 0), new Tile(2695, 9564, 0), new Tile(2691, 9564, 0), 5103, new Tile(2687,
					9564, 0), new Tile(2683, 9564, 0), new Tile(2680, 9564, 0), new Tile(2676, 9564, 0), new Tile(2674, 9568, 0),
					new Tile(2672, 9572, 0), new Tile(2667, 9572, 0), new Tile(2664, 9570, 0), new Tile(2660, 9568, 0), new Tile(
							2656, 9566, 0), new Tile(2652, 9564, 0), new Tile(2650, 9562, 0), 5110, new Tile(2647, 9556, 0),
					new Tile(2647, 9551, 0), new Tile(2647, 9547, 0), new Tile(2644, 9542, 0), new Tile(2644, 9537, 0), new Tile(
							2644, 9533, 0), new Tile(2642, 9529, 0), new Tile(2641, 9524, 0), new Tile(2641, 9520, 0), new Tile(
							2643, 9516, 0), new Tile(2646, 9512, 0), new Tile(2650, 9507, 0), new Tile(2653, 9504, 0), new Tile(
							2656, 9500, 0), new Tile(2656, 9494, 0), new Tile(2654, 9489, 0), new Tile(2655, 9483, 0), new Tile(
							2660, 9483, 0), new Tile(2664, 9483, 0), new Tile(2669, 9481, 0), new Tile(2672, 9480, 0), 5106,
					new Tile(2680, 9478, 0), new Tile(2684, 9477, 0), new Tile(2688, 9477, 0), new Tile(2691, 9480, 0), new Tile(
							2693, 9482, 0), 5107, new Tile(2698, 9482, 0), new Tile(2703, 9482, 0), new Tile(2707, 9479, 0),
					new Tile(2708, 9473, 0), new Tile(2708, 9467, 0), new Tile(2708, 9461, 0), new Tile(2708, 9456, 0), new Tile(
							2709, 9454, 0)), "Melee"),

	LESSER_DEMONS("Lesser Demons", 1, new int[] { 82 }, new CustomPath(Teleports.TAVERLEY_DUNGEON, 1759, new Tile(2884, 9800, 0),
			new Tile(2884, 9804, 0), new Tile(2884, 9810, 0), new Tile(2884, 9813, 0), new Tile(2884, 9816, 0), new Tile(2884,
					9819, 0), new Tile(2884, 9824, 0), new Tile(2885, 9828, 0), new Tile(2885, 9831, 0), new Tile(2885, 9834, 0),
			new Tile(2885, 9838, 0), new Tile(2884, 9842, 0), new Tile(2887, 9843, 0), new Tile(2889, 9845, 0), new Tile(2892,
					9848, 0), new Tile(2896, 9848, 0), new Tile(2900, 9848, 0), new Tile(2903, 9848, 0), new Tile(2907, 9848, 0),
			new Tile(2909, 9849, 0), new Tile(2913, 9849, 0), new Tile(2918, 9849, 0), new Tile(2923, 9846, 0), new Tile(2926,
					9843, 0), new Tile(2929, 9841, 0), new Tile(2932, 9838, 0), new Tile(2935, 9834, 0), new Tile(2937, 9830, 0),
			new Tile(2937, 9827, 0), new Tile(2937, 9825, 0), new Tile(2937, 9820, 0), new Tile(2937, 9817, 0), new Tile(2937,
					9814, 0), new Tile(2938, 9810, 0), new Tile(2939, 9807, 0), new Tile(2941, 9803, 0), new Tile(2944, 9799, 0),
			new Tile(2946, 9798, 0), new Tile(2948, 9796, 0), new Tile(2951, 9793, 0), new Tile(2951, 9789, 0), new Tile(2951,
					9784, 0), new Tile(2951, 9781, 0), new Tile(2952, 9779, 0), new Tile(2950, 9775, 0), new Tile(2946, 9774, 0),
			new Tile(2941, 9777, 0), new Tile(2938, 9778, 0), new Tile(2935, 9774, 0), new Tile(2935, 9771, 0), new Tile(2935,
					9768, 0), new Tile(2935, 9764, 0), new Tile(2936, 9759, 0), new Tile(2934, 9756, 0), new Tile(2928, 9755, 0),
			new Tile(2923, 9755, 0), new Tile(2922, 9759, 0), new Tile(2923, 9763, 0), new Tile(2924, 9767, 0), new Tile(2928,
					9771, 0), new Tile(2929, 9773, 0), new Tile(2931, 9776, 0), new Tile(2931, 9780, 0), new Tile(2933, 9783, 0),
			new Tile(2935, 9787, 0), new Tile(2935, 9791, 0), new Tile(2934, 9795, 0)), "Melee"),

	JELLYS("Jellys", 52, new int[] { 1637 }, new CustomPath(Teleports.FREMENNIK_SLAYER_DUNGEON, 4499, new Tile(2804, 10001, 0),
			new Tile(2801, 10001, 0), new Tile(2797, 9999, 0), new Tile(2793, 9997, 0), new Tile(2790, 9997, 0), new Tile(2787,
					9996, 0), new Tile(2784, 9996, 0), new Tile(2780, 10000, 0), new Tile(2778, 10004, 0), new Tile(2777, 10009,
					0), new Tile(2778, 10013, 0), new Tile(2783, 10016, 0), new Tile(2787, 10017, 0), new Tile(2791, 10018, 0),
			new Tile(2795, 10018, 0), new Tile(2799, 10020, 0), new Tile(2802, 10025, 0), new Tile(2799, 10029, 0), new Tile(
					2796, 10032, 0), new Tile(2792, 10033, 0), new Tile(2786, 10033, 0), new Tile(2782, 10034, 0), new Tile(2778,
					10034, 0), new Tile(2774, 10032, 0), new Tile(2769, 10034, 0), new Tile(2765, 10036, 0), new Tile(2761,
					10034, 0), new Tile(2759, 10030, 0), new Tile(2759, 10027, 0), new Tile(2760, 10023, 0), new Tile(2764,
					10019, 0), new Tile(2764, 10015, 0), new Tile(2763, 10011, 0), new Tile(2761, 10008, 0), new Tile(2759,
					10004, 0), new Tile(2759, 9999, 0), new Tile(2754, 9996, 0), new Tile(2750, 9996, 0),
			new Tile(2746, 9999, 0), new Tile(2746, 10002, 0), new Tile(2746, 10007, 0), new Tile(2745, 10010, 0), new Tile(2745,
					10015, 0), new Tile(2745, 10019, 0), new Tile(2745, 10023, 0), new Tile(2745, 10027, 0), new Tile(2744,
					10030, 0), new Tile(2738, 10031, 0), new Tile(2735, 10030, 0), new Tile(2732, 10028, 0), new Tile(2731,
					10025, 0), new Tile(2729, 10021, 0), new Tile(2724, 10022, 0), new Tile(2723, 10025, 0), new Tile(2721,
					10028, 0), new Tile(2717, 10030, 0), new Tile(2712, 10030, 0), new Tile(2708, 10030, 0), new Tile(2704,
					10027, 0)), "Melee"),

	NECHRYAELS("Nechryaels", 1, new int[] { 1613 }, new CustomPath(Teleports.CHAOS_TUNNELS, new Tile(3152, 5561, 0), new Tile(
			3156, 5561, 0), 28779), "Melee"),

	PYREFIENDS("Pyrefiends", 30, new int[] { 1633 }, new CustomPath(Teleports.FREMENNIK_SLAYER_DUNGEON, 4499, new Tile(2804,
			10001, 0), new Tile(2801, 10001, 0), new Tile(2797, 9999, 0), new Tile(2793, 9997, 0), new Tile(2790, 9997, 0),
			new Tile(2787, 9996, 0), new Tile(2784, 9996, 0), new Tile(2780, 10000, 0), new Tile(2778, 10004, 0), new Tile(2777,
					10009, 0), new Tile(2778, 10013, 0), new Tile(2783, 10016, 0), new Tile(2787, 10017, 0), new Tile(2791,
					10018, 0), new Tile(2795, 10018, 0), new Tile(2799, 10020, 0), new Tile(2802, 10025, 0), new Tile(2799,
					10029, 0), new Tile(2796, 10032, 0), new Tile(2792, 10033, 0), new Tile(2786, 10033, 0), new Tile(2782,
					10034, 0), new Tile(2778, 10034, 0), new Tile(2774, 10032, 0), new Tile(2769, 10034, 0), new Tile(2765,
					10036, 0), new Tile(2761, 10034, 0), new Tile(2759, 10030, 0), new Tile(2759, 10027, 0), new Tile(2760,
					10023, 0), new Tile(2764, 10019, 0), new Tile(2764, 10015, 0), new Tile(2763, 10011, 0), new Tile(2761,
					10008, 0), new Tile(2759, 10004, 0)), "None"),

	GREATER_DEMONS("Greater Demons", 70, new int[] { 83 }, new CustomPath(Teleports.BRIMHAVEN_DUNGEON, 5083, new Tile(2709, 9564,
			0), new Tile(2705, 9564, 0), new Tile(2702, 9564, 0), new Tile(2698, 9564, 0), new Tile(2695, 9564, 0), new Tile(
			2691, 9564, 0), 5103, new Tile(2687, 9564, 0), new Tile(2683, 9564, 0), new Tile(2680, 9564, 0), new Tile(2676, 9564,
			0), new Tile(2674, 9568, 0), new Tile(2672, 9572, 0), new Tile(2667, 9572, 0), new Tile(2664, 9570, 0), new Tile(
			2660, 9568, 0), new Tile(2656, 9566, 0), new Tile(2652, 9564, 0), new Tile(2650, 9562, 0), 5110, new Tile(2647, 9555,
			0), new Tile(2647, 9551, 0), new Tile(2647, 9548, 0), new Tile(2647, 9542, 0), new Tile(2646, 9540, 0), new Tile(
			2645, 9536, 0), new Tile(2643, 9532, 0), new Tile(2642, 9529, 0), new Tile(2639, 9525, 0), new Tile(2638, 9523, 0),
			new Tile(2638, 9520, 0), new Tile(2637, 9518, 0), 5097, new Tile(2637, 9508, 2), new Tile(2637, 9502, 2)), "Melee"),

	TUROTHS("Turoths", 25, new int[] { 1628, 1627, 1623 }, new CustomPath(Teleports.FREMENNIK_SLAYER_DUNGEON, 4499, new Tile(
			2804, 10001, 0), new Tile(2801, 10001, 0), new Tile(2797, 9999, 0), new Tile(2793, 9997, 0), new Tile(2790, 9997, 0),
			new Tile(2787, 9996, 0), new Tile(2784, 9996, 0), new Tile(2780, 10000, 0), new Tile(2778, 10004, 0), new Tile(2777,
					10009, 0), new Tile(2778, 10013, 0), new Tile(2783, 10016, 0), new Tile(2787, 10017, 0), new Tile(2791,
					10018, 0), new Tile(2795, 10018, 0), new Tile(2799, 10020, 0), new Tile(2802, 10025, 0), new Tile(2799,
					10029, 0), new Tile(2796, 10032, 0), new Tile(2792, 10033, 0), new Tile(2786, 10033, 0), new Tile(2782,
					10034, 0), new Tile(2778, 10034, 0), new Tile(2774, 10032, 0), new Tile(2769, 10034, 0), new Tile(2765,
					10036, 0), new Tile(2761, 10034, 0), new Tile(2759, 10030, 0), new Tile(2759, 10027, 0), new Tile(2760,
					10023, 0), new Tile(2764, 10019, 0), new Tile(2764, 10015, 0), new Tile(2763, 10011, 0), new Tile(2761,
					10008, 0), new Tile(2759, 10004, 0), new Tile(2759, 9999, 0), new Tile(2754, 9996, 0),
			new Tile(2750, 9996, 0), new Tile(2746, 9999, 0), new Tile(2746, 10002, 0), new Tile(2746, 10007, 0), new Tile(2745,
					10010, 0), new Tile(2745, 10015, 0), new Tile(2745, 10019, 0), new Tile(2745, 10023, 0), new Tile(2745,
					10027, 0), new Tile(2744, 10030, 0), new Tile(2738, 10031, 0), new Tile(2735, 10030, 0), new Tile(2732,
					10028, 0), new Tile(2731, 10025, 0), new Tile(2729, 10021, 0), new Tile(2724, 10022, 0), new Tile(2723,
					10025, 0), new Tile(2721, 10028, 0), new Tile(2717, 10030, 0), new Tile(2712, 10030, 0), new Tile(2708,
					10030, 0), new Tile(2704, 10027, 0), new Tile(2704, 10022, 0), new Tile(2704, 10017, 0), new Tile(2708,
					10015, 0), new Tile(2712, 10013, 0), new Tile(2717, 10010, 0), new Tile(2721, 10006, 0), new Tile(2724,
					10003, 0)), "Melee"),

	SKELETONS("Skeletons", 1, new int[] { 93, 92 }, new CustomPath(Teleports.TAVERLEY_DUNGEON, 1759, new Tile(2884, 9800, 0),
			new Tile(2884, 9803, 0), new Tile(2884, 9806, 0), new Tile(2884, 9809, 0), new Tile(2884, 9812, 0), new Tile(2884,
					9816, 0)), "None"),

	WATER_FIENDS("Water fiends", 50, new int[] { 5361 }, new CustomPath(Teleports.ANCIENT_CAVERN, new Tile(1747, 5328, 0),
			new Tile(1747, 5332, 0), new Tile(1747, 5335, 0), new Tile(1745, 5339, 0), new Tile(1742, 5341, 0), new Tile(1740,
					5343, 0)), "Melee");

	private String name;
	private int[] npcIds;
	private int slayerLevel;
	private CustomPath path;
	private String overHead;

	Monster(String name, int slayerLevel, int[] npcIds, CustomPath path, String overHead) {
		this.name = name;
		this.npcIds = npcIds;
		this.slayerLevel = slayerLevel;
		this.path = path;
		this.overHead = overHead;
	}

	public String getPrayerType() {
		return overHead;
	}

	public String getName() {
		return name;
	}

	public int[] getNpcIds() {
		return npcIds;
	}

	public int getSlayerLevel() {
		return slayerLevel;
	}

	public CustomPath getPath() {
		return path;
	}

}