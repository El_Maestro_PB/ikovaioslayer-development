package com.ikovps.slayer.data;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.Loader;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.Tile;

import com.ikovps.slayer.strategies.HandleCombat;
import com.ikovps.slayer.util.Client;
import com.ikovps.slayer.util.CustomPath;
import com.ikovps.slayer.util.Teleports;

public enum SlayerMaster {

	MAZCHNA("Mazchna", new CustomPath(Teleports.HOME, new Tile(3088, 3500, 0), new Tile(3088, 3497, 0), new Tile(3088, 3493, 0),
			new Tile(3089, 3489, 0), new Tile(3092, 3485, 0), new Tile(3094, 3483, 0)), 8274), VANNAKA("Vannaka", new CustomPath(
			Teleports.SLAYER_TOWER, new Tile(3430, 3538, 0)), 1597), CHAELDAR("Chaeldar", new CustomPath(Teleports.CAMELOT,
			new Tile(2755, 3478, 0), new Tile(2751, 3478, 0), new Tile(2747, 3478, 0), new Tile(2745, 3478, 0), new Tile(2739,
					3480, 0), new Tile(2736, 3482, 0), new Tile(2732, 3483, 0), new Tile(2728, 3484, 0), new Tile(2724, 3484, 0),
			new Tile(2721, 3484, 0), new Tile(2717, 3484, 0)), 1598), DURADEL("Duradel", new CustomPath(
			Teleports.BRIMHAVEN_DUNGEON, new Tile(2744, 3150, 0)), 8275);

	private String name;
	private CustomPath path;
	private int id;

	SlayerMaster(String name, CustomPath path, int id) {
		this.name = name;
		this.path = path;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void travelTo() {
		if (path.isTraversable()) {
			Variables.setStatus("Going to slayer master: " + name + "...", true);
			path.traverse();
			Time.sleep(1000);
		}
	}

	public void getTask() {
		if (Variables.getTask() == null || Variables.getTask().isDone()) {
			switch (Game.getOpenBackDialogId()) {
			case -1:
				Npc[] master = {};
				try {
					master = Npcs.getNearest(id);
				} catch (NullPointerException e) {
					e.printStackTrace();
				}
				if (master.length > 0 && master[0] != null && Game.getOpenBackDialogId() != 4887) {
					master[0].interact(0);
					Time.sleep(new SleepCondition() {
						@Override
						public boolean isValid() {
							return Game.getOpenBackDialogId() == 4887;
						}
					}, 5000);
				}
				break;
			case 4887:
				boolean isTask = Loader.getClient().getInterfaceCache()[4890].getMessage().toLowerCase().startsWith("you");
				boolean isOldTask = Loader.getClient().getInterfaceCache()[4890].getMessage().toLowerCase().contains("already");
				if (isTask) {
					if (!isOldTask) {
						String getTask = Loader.getClient().getInterfaceCache()[4890].getMessage();
						String[] i = getTask.split("kill ");
						for (Monster s : Monster.values()) {
							if (Variables.isCamping()) {
								if (getTask.toLowerCase().contains(Variables.getMonsterToCamp())) {
									String[] z = i[1].toLowerCase().split(" " + s.getName().toLowerCase());
									Variables.setTask(new Task(s, Integer.parseInt(z[0])));
									Variables.setStatus("Task set to: " + Variables.getTask().getKillsLeft() + " "
											+ Variables.getTask().getMonsterName(), true);
									HandleCombat.monsterToFight = null;
								}
							} else {
								if (getTask.toLowerCase().contains(s.getName().toLowerCase())) {
									String[] z = i[1].toLowerCase().split(" " + s.getName().toLowerCase());
									Variables.setTask(new Task(s, Integer.parseInt(z[0])));
									Variables.setStatus("Task set to: " + Variables.getTask().getKillsLeft() + " "
											+ Variables.getTask().getMonsterName(), true);
									HandleCombat.monsterToFight = null;
								}
							}
						}
						Menu.sendAction(679, 38, 320, 4892, 398);
						Time.sleep(new SleepCondition() {
							@Override
							public boolean isValid() {
								return Game.getOpenBackDialogId() != 4887;
							}
						}, 5000);
						Players.getMyPlayer().getLocation().walkTo();
						Time.sleep(500);
					} else {
						Variables.setResetTask(true);
						Menu.sendAction(679, 38, 320, 4892, 398);
						Time.sleep(new SleepCondition() {
							@Override
							public boolean isValid() {
								return Game.getOpenBackDialogId() != 4887;
							}
						}, 5000);
					}
					break;
				}
				Menu.sendAction(679, 38, 320, 4892, 398);
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() == 2492 || Game.getOpenBackDialogId() == 2469;
					}
				}, 5000);
			case 2469:
				if (Variables.isResetTask()) {
					Variables.setStatus("Resetting task...", false);
					Client.clickTripleOption(3);
					Variables.setResetTask(false);
				} else {
					Client.clickTripleOption(1);
				}
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() != 2469;
					}
				}, 5000);
				break;
			case 2492:
				if (Variables.isResetTask()) {
					Variables.setStatus("Resetting task...", false);
					Client.clickQuintupleOption(4);
					Variables.setResetTask(false);
				} else {
					Client.clickQuintupleOption(1);
				}
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Game.getOpenBackDialogId() != 2492;
					}
				}, 5000);
				break;
			}
		}
	}

	public CustomPath getPath() {
		return path;
	}

	public int getId() {
		return id;
	}

	public boolean atMaster() {
		return Npcs.getNearest(id).length > 0 && Npcs.getNearest(id) != null;

	}

}