package com.ikovps.slayer.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.parabot.core.ui.Logger;

import com.ikovps.slayer.util.Consumable;

public class Variables {

	private static SlayerMaster master;
	private static String status;
	private static boolean hasTask, resetTask, camping, specialAttack, paintIds, overheads, pray, showPaint;
	private static Task task;
	private static HashMap<Consumable, Integer[]> consumables = new HashMap<Consumable, Integer[]>();
	private static int tasksCompleted, specialWeapon, primaryWeapon, specialAttackAmountNeed, monstersKilled, pointsEarned;
	private static int[] lootIds = Constants.DEFAULT_LOOT;
	private static String monsterToCamp = null;
	private static ArrayList<Prayer> prayers = new ArrayList<Prayer>();

	public static String getMonsterToCamp() {
		return monsterToCamp;
	}

	public static void setMonsterToCamp(String monsterToCamp) {
		Variables.monsterToCamp = monsterToCamp;
	}

	public static SlayerMaster getMaster() {
		return master;
	}

	public static void setMaster(SlayerMaster master) {
		Variables.master = master;
	}

	public static String getStatus() {
		return status;
	}

	public static void setStatus(String status, boolean uliratha) {
		if (Variables.status != status) {
			Variables.status = status;
			Logger.addMessage(status, uliratha);
		}
	}

	public static boolean isHasTask() {
		return hasTask;
	}

	public static void setHasTask(boolean hasTask) {
		Variables.hasTask = hasTask;
	}

	public static boolean isResetTask() {
		return resetTask;
	}

	public static void setResetTask(boolean resetTask) {
		Variables.resetTask = resetTask;
	}

	public static Task getTask() {
		return task;
	}

	public static void setTask(Task task) {
		Variables.task = task;
	}

	public static int getTasksCompleted() {
		return tasksCompleted;
	}

	public static void setTasksCompleted(int tasksCompleted) {
		Variables.tasksCompleted = tasksCompleted;
	}

	public static HashMap<Consumable, Integer[]> getConsumables() {
		return consumables;
	}

	public static void setConsumables(HashMap<Consumable, Integer[]> consumables) {
		Variables.consumables = consumables;
	}

	public static void addConsumable(Consumable consumable, Integer[] amount) {
		consumables.put(consumable, amount);

	}

	public static boolean isCamping() {
		return camping;
	}

	public static void setCamping(boolean camping) {
		Variables.camping = camping;
	}

	public static int[] getLoot() {
		return lootIds;
	}

	public static void setLoot(int[] loot) {
		lootIds = loot;
	}

	public static void addLoot(int[] loot) {
		int[] s = new int[lootIds.length + loot.length];
		for (int i = 0; i < s.length; i++) {
			if (i < lootIds.length) {
				s[i] = lootIds[i];
			} else {
				s[i] = loot[i - lootIds.length];
			}
		}
		lootIds = s;
	}

	public static int[] getLootInInventory() {
		int[] s = lootIds;
		for (int i = 0; i < s.length; i++) {
			s[i] = s[i] + 1;
		}
		return s;
	}

	public static int getPrimaryWeapon() {
		return primaryWeapon;
	}

	public static void setPrimaryWeapon(int primaryWeapon) {
		Variables.primaryWeapon = primaryWeapon;
	}

	public static int getSpecialWeapon() {
		return specialWeapon;
	}

	public static void setSpecialWeapon(int specialWeapon) {
		Variables.specialWeapon = specialWeapon;
	}

	public static boolean isSpecialAttack() {
		return specialAttack;
	}

	public static void setSpecialAttack(boolean specialAttack) {
		Variables.specialAttack = specialAttack;
	}

	public static int getSpecialAttackAmountNeed() {
		return specialAttackAmountNeed;
	}

	public static void setSpecialAttackAmountNeed(int specialAttackAmountNeed) {
		Variables.specialAttackAmountNeed = specialAttackAmountNeed;
	}

	public static boolean isPaintIds() {
		return paintIds;
	}

	public static void setPaintIds(boolean paintIds) {
		Variables.paintIds = paintIds;
	}

	public static boolean isShowPaint() {
		return showPaint;
	}

	public static void setShowPaint(boolean showPaint) {
		Variables.showPaint = showPaint;
	}

	public static boolean isPray() {
		return pray;
	}

	public static void setPray(boolean pray) {
		Variables.pray = pray;
	}

	public static boolean isOverheads() {
		return overheads;
	}

	public static void setOverheads(boolean overheads) {
		Variables.overheads = overheads;
	}

	public static ArrayList<Prayer> getPrayers() {
		return prayers;
	}

	public static void setPrayers(ArrayList<Prayer> prayers) {
		Variables.prayers = prayers;
	}

	public static void addPrayer(Prayer i) {
		prayers.add(i);
	}

	public static int getMonstersKilled() {
		return monstersKilled;
	}

	public static void setMonstersKilled(int monstersKilled) {
		Variables.monstersKilled = monstersKilled;
	}

	public static int getPointsEarned() {
		return pointsEarned;
	}

	public static void setPointsEarned(int pointsEarned) {
		Variables.pointsEarned = pointsEarned;
	}

	public static Food getFood() {
		for (Entry<Consumable, Integer[]> entry : Variables.getConsumables().entrySet()) {
			if (entry.getValue().length > 1) {
				return (Food) entry.getKey();
			}
		}
		return null;
	}

}