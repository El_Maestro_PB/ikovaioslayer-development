package com.ikovps.slayer.data;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.wrappers.Item;

import com.ikovps.slayer.util.Consumable;
import com.ikovps.slayer.util.Utils;

public enum Potion implements Consumable {

	PRAYER_RENEWAL_POTION("Prayer Renewal Potion", Skill.PRAYER, new int[] { 21637, 21635, 21633, 21631 }), SUPER_RESTORE_POTION(
			"Super Restore Potion", Skill.PRAYER, new int[] { 3031, 3029, 3027, 3025 }), PRAYER_POTION("Prayer Potion",
			Skill.PRAYER, new int[] { 144, 142, 140, 2435 }), SUPER_DEFENSE_POTION("Super Defense Potion", Skill.DEFENSE,
			new int[] { 168, 166, 164, 2443 }), SUPER_ATTACK_POTION("Super Attack Potion", Skill.ATTACK, new int[] { 150, 148,
			146, 2437 }), SUPER_STRENGTH_POTION("Super Strength Potion", Skill.STRENGTH, new int[] { 162, 160, 158, 2441 }), PRAYER_RENEWAL_FLASK(
			"Prayer Renewal Flask", Skill.PRAYER, new int[] { 14280, 14282, 14284, 14286, 14288, 14290 }), SUPER_RESTORE_FLASK(
			"Super Restore Flask", Skill.PRAYER, new int[] { 14406, 14408, 14410, 14412, 14414, 14416 }), PRAYER_FLASK(
			"Prayer Flask", Skill.PRAYER, new int[] { 14191, 14193, 14195, 14197, 14199, 14201 }), SUPER_DEFENSE_FLASK(
			"Super Defense Flask", Skill.DEFENSE, new int[] { 14155, 14157, 14159, 14161, 14163, 14165 }), SUPER_ATTACK_FLASK(
			"Super Attack Flask", Skill.ATTACK, new int[] { 14179, 14181, 14183, 14185, 14187, 14189 }), SUPER_STRENGTH_FLASK(
			"Super Strength Flask", Skill.STRENGTH, new int[] { 14167, 14169, 14171, 14173, 14175, 14177 });

	private int[] ids;
	private String name;
	private Skill skill;

	Potion(String name, Skill skill, int... ids) {
		this.ids = ids;
		this.name = name;
		this.skill = skill;
	}

	public boolean isFlask() {
		return name.toLowerCase().contains("flask");
	}

	public String getName() {
		return name;
	}

	public int[] getIds() {
		return ids;
	}

	public int getIdByDose(int dose) {
		if (dose > ids.length) {
			return ids[ids.length - 1];
		}
		return ids[dose - 1];

	}

	public int getCount() {
		return Inventory.getCount(ids);
	}

	@Override
	public void consume() {
		if (Inventory.getItems(ids) != null) {
			for (Item i : Inventory.getItems(ids)) {
				if (i != null) {
					Menu.sendAction(74, i.getId() - 1, i.getSlot(), 3214);
					Time.sleep(new SleepCondition() {
						@Override
						public boolean isValid() {
							return !needToConsume();
						}
					}, 1500);
				}
			}
		}

	}

	@Override
	public boolean hasInBag() {
		return Inventory.getCount(ids) > 0;
	}

	@Override
	public boolean needToConsume() {
		if (Skill.PRAYER == skill) {
			return skill.getLevel() < Utils.getRealLevel(skill) / 2;
		}
		return skill.getLevel() < Utils.getRealLevel(skill) * 1.10;
	}

	@Override
	public boolean needBankForMore() {
		return Inventory.getCount(ids) < 1;
	}

}
