package com.ikovps.slayer.data;

import java.util.Map.Entry;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Item;

import com.ikovps.slayer.util.Consumable;
import com.ikovps.slayer.util.Utils;

public enum Food implements Consumable {

	SHARK("Shark", 386), MONKFISH("Monkfish", 7947), LOBSTER("Lobster", 380), ROCKTAIL("Rocktail", 15273);

	private int id;
	private String name;

	Food(String name, int id) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getCount() {
		return Inventory.getCount(id);
	}

	@Override
	public void consume() {
		if (getCount() > 0) {
			Item[] food = Inventory.getItems(id);
			if (food.length > 0 && food[0] != null) {
				Menu.sendAction(74, food[0].getId() - 1, food[0].getSlot(), 3214);
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return !needToConsume();
					}
				}, 1000);
			}
		}

	}

	@Override
	public boolean hasInBag() {
		return Inventory.getCount(id) > 0;
	}

	@Override
	public boolean needToConsume() {
		for (Entry<Consumable, Integer[]> entry : Variables.getConsumables().entrySet()) {
			if (entry.getValue().length > 1) {
				return Utils.getHpPercent() <= entry.getValue()[1];
			}
		}
		return false;
	}

	@Override
	public boolean needBankForMore() {
		return getCount() < 1;
	}
}
