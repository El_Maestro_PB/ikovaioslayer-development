package com.ikovps.slayer.data;

public class Constants {
	public final static double VERSION = 1.6;
	public final static int VIAL = 230;
	public final static int CASKET = 7957;
	public final static int COINS = 996;
	public final static int[] ANTI_FIRESHIELDS = { 11284, 1541 };
	public final static int[] DEFAULT_LOOT = { 11286, 536, 4151, 7956, 986, 12158, 12159, 12160, 12163, 985, 986, 987, 988, 6798,
			6799, 6800, 6801, 6802, 6803, 6804, 6805, 6806, 6807, 6808 };

}
