package com.ikovps.slayer.util;

import java.lang.reflect.Field;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.Loader;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.wrappers.Item;

/**
 * 
 * @author Bautista
 *
 */
public enum Equipment {

	HELMET(0), CAPE(1), NECKLACE(2), WEAPON(3), TOP(4), SHIELD(5), LEGS(7), GLOVES(9), BOOTS(10);

	private int slotIndex;

	Equipment(int slotIndex) {
		this.slotIndex = slotIndex;
	}

	public void remove() {
		if (getItemId() != 0) {
			Menu.sendAction(632, getItemId() - 1, slotIndex, 1688);
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return getItemId() <= 0;
				}
			}, 2000);
		}
	}

	public void equip(final int itemId) {
		if (!isWearing(itemId)) {
			for (Item item : Inventory.getItems(itemId)) {
				if (item != null) {
					Menu.sendAction(454, item.getId() - 1, item.getSlot(), 3214);
					break;
				}
			}
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return isWearing(itemId);
				}
			}, 2000);
		}
	}

	public boolean isWearing(int itemId) {
		return getItemId() == itemId;
	}

	private int getItemId() {
		return (getEquipment()[slotIndex] - 511);
	}

	private int[] getEquipment() {
		try {
			Field equipment = Loader.getClient().getMyPlayer().getClass().getDeclaredField("aW");
			equipment.setAccessible(true);
			return (int[]) equipment.get(Loader.getClient().getMyPlayer());
		} catch (NoSuchFieldException | IllegalAccessException e) {
			return null;
		}
	}
}
