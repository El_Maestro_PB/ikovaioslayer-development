package com.ikovps.slayer.util;

import org.parabot.environment.api.utils.Timer;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Skill;

/**
 * A wrapper class used to time a skill.
 * 
 * @author Empathy
 *
 */
public class ScriptTimer extends Timer {

	/**
	 * The skill to time.
	 */
	private final Skill skill;

	/**
	 * The start exp.
	 */
	private final int startExp;

	/**
	 * The current amount.
	 */
	private int currentAmount;

	/**
	 * The start amount.
	 */
	private int startAmount;

	/**
	 * The product.
	 */
	private int product;

	/**
	 * The start level.
	 */
	private int startLevel;

	/**
	 * Constructs a new script timer.
	 * 
	 * @param skill
	 *            the skill to time.
	 * @Param expPerProduct the exp per product.
	 */
	public ScriptTimer(Skill skill, int productId) {
		this.startLevel = skill.getRealLevel();
		this.skill = skill;
		this.product = productId;
		this.startExp = skill.getExperience();
		this.startAmount = Inventory.getCount(true, productId);
	}

	/**
	 * Constructs new script timer.
	 * 
	 * @param skill
	 *            to track
	 * 
	 * @param skill
	 */
	public ScriptTimer(Skill skill) {
		this.startLevel = skill.getRealLevel();
		this.skill = skill;
		this.startExp = skill.getExperience();
	}

	/**
	 * @return the gained amount
	 */
	public int getGainedAmount() {
		return getCurrentAmount() + getInventoryAmount() - startAmount;
	}

	/**
	 * @return the inventory amount
	 */
	public int getInventoryAmount() {
		return Inventory.getCount(true, product);
	}

	/**
	 * Sets the currentAmount
	 * 
	 * @param currentAmount
	 */
	public void setCurrentAmount(int currentAmount) {
		this.currentAmount += currentAmount;
	}

	/**
	 * @return the current amount.
	 */
	public int getCurrentAmount() {
		return currentAmount;
	}

	/**
	 * Gets the exp gained over time.
	 * 
	 * @return the exp gained.
	 */
	public int getXpGained() {
		return skill.getExperience() - startExp;
	}

	/**
	 * 
	 * @return levels gained
	 */
	public int levelsGained() {
		if (startLevel >= 99) {
			if (skill == Skill.DUNGEONEERING && startLevel < 120) {
				return skill.getRealLevel() - startLevel;
			}
			return 0;
		}
		return skill.getRealLevel() - startLevel;
	}

	/**
	 * 
	 * @return current level not affected by boosts.
	 */
	public int getCurrentRealLevel() {
		return skill.getRealLevel();
	}

	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	/**
	 * @return the startExp
	 */
	public double getStartExp() {
		return startExp;
	}

	/**
	 * Finds time till next level according to the current exp rate
	 * 
	 * @return time till next level
	 */
	private long getTimeTillNextLevel() {
		if (getCurrentRealLevel() >= 99) {
			if (skill == Skill.DUNGEONEERING && skill.getLevel() < 120) {
				return (long) ((Skill.getExperienceByLevel(skill.getRealLevel() + 1) - skill.getExperience()) / (1000 * (getXpGained() + 1) / getElapsedTime()));
			}
			return 0;
		}
		return (long) ((Skill.getExperienceByLevel(skill.getRealLevel() + 1) - skill.getExperience()) / (1000 * (getXpGained() + 1) / getElapsedTime()));
	}

	public String timeTillLevel() {
		StringBuilder b = new StringBuilder();
		long elapsed = getTimeTillNextLevel();
		int second = (int) (elapsed % 60);
		int minute = (int) (elapsed / 60 % 60);
		int hour = (int) (elapsed / 3600 % 60);
		b.append(hour < 10 ? "0" : "").append(hour).append(":");
		b.append(minute < 10 ? "0" : "").append(minute).append(":");
		b.append(second < 10 ? "0" : "").append(second);
		return new String(b);
	}
}
