package com.ikovps.slayer.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JTextArea;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.Loader;
import org.rev317.min.accessors.Interface;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Inventory;
import org.rev317.min.api.methods.Menu;
import org.rev317.min.api.methods.Npcs;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.Skill;
import org.rev317.min.api.wrappers.Item;
import org.rev317.min.api.wrappers.Npc;
import org.rev317.min.api.wrappers.Player;

import com.ikovps.slayer.data.Constants;
import com.ikovps.slayer.data.Variables;

/**
 * A utility class containing useful methods.
 * 
 * @author Masood
 *
 */
public class Utils {

	/**
	 * Deposits all items except the listed ids.
	 * 
	 * @param itemIDs
	 *            the item ids to not deposit.
	 */
	public static void depositAllExcept(int... itemIDs) {
		final ArrayList<Integer> dontDeposit = new ArrayList<Integer>();
		if (Inventory.getCount(false) <= 2) {
			return;
		} else {
			for (int i : itemIDs) {
				dontDeposit.add(i);
			}
		}
		for (Item inventoryItem : Inventory.getItems()) {
			if (!dontDeposit.contains(inventoryItem.getId())) {
				Menu.sendAction(431, inventoryItem.getId() - 1, inventoryItem.getSlot(), 5064, 2213, 3);
				Time.sleep(80);
			}
		}
	}

	/**
	 * Gets the player's hp percentage.
	 * 
	 * @return the hp percentage.
	 */
	public static int getHpPercent() {
		if (Skill.HITPOINTS.getRealLevel() > 99) {
			return (Skill.HITPOINTS.getLevel() * 100) / 99;
		} else {
			return (Skill.HITPOINTS.getLevel() * 100) / Skill.HITPOINTS.getRealLevel();
		}
	}

	// Drop All Except
	public static void dropAllExcept(int... itemIDs) {
		final ArrayList<Integer> dontDrop = new ArrayList<>();
		if (Inventory.getCount(false) <= 2) {
			return;
		} else {
			for (int i : itemIDs) {
				dontDrop.add(i);
			}
		}
		for (Item inventoryItem : Inventory.getItems()) {
			if (!dontDrop.contains(inventoryItem.getId())) {
				Menu.sendAction(847, inventoryItem.getId() - 1, inventoryItem.getSlot(), 3214);
				Time.sleep(80);
			}
		}
	}

	public static void toggleRetaliate() {
		if (retaliateIsActivated()) {
			Menu.sendAction(169, 551, 0, 150, 389);
			Time.sleep(new SleepCondition() {
				@Override
				public boolean isValid() {
					return !retaliateIsActivated();
				}
			}, 500);
			return;
		}
		Menu.sendAction(169, 551, 0, 150, 389);
		Time.sleep(new SleepCondition() {
			@Override
			public boolean isValid() {
				return retaliateIsActivated();
			}
		}, 500);
	}

	public static boolean retaliateIsActivated() {
		return Game.getSetting(172) != 0;
	}

	/**
	 * Formats a number as a string.
	 * 
	 * @param number
	 *            the number to format.
	 * @return the string formatted.
	 */
	public static String formatNumber(int start) {
		DecimalFormat nf = new DecimalFormat("0.0");
		double i = start;
		if (i >= 1000000) {
			return nf.format((i / 1000000)) + "M";
		}
		if (i >= 1000) {
			return nf.format((i / 1000)) + "K";
		}
		return "" + start;
	}

	/**
	 * @author Ethan
	 * @return if in combat.
	 */
	public static boolean isFighting() {
		try {
			Npc[] npcs = Npcs.getNpcs();
			for (Npc npc : npcs) {
				if (npc != null) {
					if (Players.getMyPlayer().getInteractingCharacter() != null
							&& Players.getMyPlayer().getInteractingCharacter().isInCombat()) {
						if (Players.getMyPlayer() != null) {
							if (Players.getMyPlayer().getInteractingCharacter().equals(npc) && Players.getMyPlayer().isInCombat()) {
								return true;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			Variables.setStatus("isFighting error", true);
		}
		return false;
	}

	public static boolean canSpecial() {
		return (Game.getSetting(300) / 10) >= Variables.getSpecialAttackAmountNeed();
	}

	public static boolean isSpecActivated() {
		String message;
		Interface inter;
		return (inter = Loader.getClient().getInterfaceCache()[7561]) != null && (message = inter.getMessage()) != null
				&& message.startsWith("@yel@");
	}

	public static void handleSpecialAttack() {
		if (canSpecial()) {
			if (!Equipment.WEAPON.isWearing(Variables.getSpecialWeapon())) {
				Equipment.WEAPON.equip(Variables.getSpecialWeapon());
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return Equipment.WEAPON.isWearing(Variables.getSpecialWeapon());
					}
				}, 2000);
			}
			if (Equipment.WEAPON.isWearing(Variables.getSpecialWeapon()) && !isSpecActivated()) {
				Menu.sendAction(315, 0, 0, 7562, 2213, 1);
				Time.sleep(new SleepCondition() {
					@Override
					public boolean isValid() {
						return isSpecActivated();
					}
				}, 2000);
			}
		}
	}

	public static int getRealLevel(Skill skill) {
		if (skill.getRealLevel() <= 99) {
			return skill.getRealLevel();
		} else if (skill.getRealLevel() > 99) {
			return 99;
		}
		return -1;
	}

	public static void loadInfo(JTextArea i) {
		try {
			URL text = new URL("https://dl.dropboxusercontent.com/s/x2dkwnl27kwxdc3/IkovAIOSlayer%20Announcement.txt?dl=0");
			BufferedReader textFile = new BufferedReader(new InputStreamReader(text.openStream()));
			StringBuffer message = new StringBuffer();
			String textLine;
			String currentPhrase = "";
			while ((textLine = textFile.readLine()) != null) {
				if (textLine.length() <= 40) {
					message.append(textLine).append(System.getProperty("line.separator"));
				} else {
					for (int wordIndex = 0; wordIndex < textLine.split(" ").length; wordIndex++) {
						String newPhrase = textLine.split(" ")[wordIndex] + " ";
						if ((currentPhrase + newPhrase).length() <= 40) {
							currentPhrase = currentPhrase + (textLine.split(" ")[wordIndex] + " ");
						} else {
							message.append(currentPhrase + System.getProperty("line.separator"));
							currentPhrase = textLine.split(" ")[wordIndex] + " ";
						}
						if (textLine.endsWith(textLine.split(" ")[wordIndex])) {
							message.append(currentPhrase + System.getProperty("line.separator"));
							currentPhrase = "";
						}
					}
				}
			}
			textFile.close();
			i.setText(message.toString());
		} catch (IOException io) {
			i.setText("Failed to load announcements.");
			io.printStackTrace();
		}
	}

	public static boolean npcIsInCombatWithOther(Npc monster) {
		try {
			Player[] players = Players.getPlayers();
			for (Player player : players) {
				if (player != null && player.getName() != Players.getMyPlayer().getName()) {
					if (monster != null) {
						if (monster.getInteractingCharacter() != null) {
							if (monster.getInteractingCharacter().equals(player)) {
								return true;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			Variables.setStatus("npcIsInCombatWithOther error", true);
		}
		return false;
	}

	public static boolean needAntiShield() {
		return Inventory.getItems(Constants.ANTI_FIRESHIELDS).length <= 0
				&& !Equipment.SHIELD.isWearing(Constants.ANTI_FIRESHIELDS[0])
				&& !Equipment.SHIELD.isWearing(Constants.ANTI_FIRESHIELDS[1]);
	}

	public static int[] getTabInterfaceIDs() {
		return Reflection.getTabInterfaceIDs();
	}

	public static boolean isWalking() {
		return Reflection.walking() != 0;
	}

	public static void dropClient() {
		Reflection.dropClient();
	}

}