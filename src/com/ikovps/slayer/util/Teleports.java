package com.ikovps.slayer.util;

import org.parabot.environment.api.utils.Time;
import org.rev317.min.api.methods.Menu;

/**
 * An enum containing all teleports for Ikov.
 * 
 * @author El Maestro
 *
 */

public enum Teleports {

	ROCK_CRABS(new int[] { 1164, 13035, 30064 }, 2494), EXPERIMENTS(new int[] { 1164, 13035, 30064 }, 2495), YAKS(new int[] {
			1164, 13035, 30064 }, 2496), BANDITS(new int[] { 1164, 13035, 30064 }, 2497), GHOULS(
			new int[] { 1164, 13035, 30064 }, 2498, 2494), CHAOS_DRUIDS(new int[] { 1164, 13035, 30064 }, 2498, 2495), TZHAAR(
			new int[] { 1164, 13035, 30064 }, 2498, 2496), DUST_DEVILS(new int[] { 1164, 13035, 30064 }, 2498, 2497), CHICKENS(
			new int[] { 1164, 13035, 30064 }, 2498, 2498, 2494), MONKEY_SKELETONS(new int[] { 1164, 13035, 30064 }, 2498, 2498,
			2495), APE_ATOLL_MONKEY_GUARDS(new int[] { 1164, 13035, 30064 }, 2498, 2498, 2496), ARMOURED_ZOMBIES(new int[] {
			1164, 13035, 30075 }, 2498, 2498, 2497), BARROWS(new int[] { 1167, 13045, 30075 }, 2494), DUEL_AREANA(new int[] {
			1164, 13045, 30075 }, 2495), PEST_CONTROL(new int[] { 1167, 13045, 30075 }, 2496), WARRIOR_GUILD(new int[] { 1167,
			13045, 30075 }, 2497), FIGHT_CAVES(new int[] { 1167, 13045, 30075 }, 2498, 2494), CASTLE_WARS(new int[] { 1167,
			13045, 30075 }, 2498, 2495), FIGHT_PITS(new int[] { 1167, 13045, 30075 }, 2498, 2496), CHAMPIONS_CHALLENGE(new int[] {
			1167, 13045, 30075 }, 2498, 2497), CLAN_WARS(new int[] { 1167, 13045, 30075 }, 2498, 2498), EDGVILLE(new int[] {
			1170, 13079, 30083 }, 2471, 2494), EAST_DRAGONS(new int[] { 1170, 13079, 30083 }, 2471, 2495), MAGE_BANK(new int[] {
			1170, 13079, 30083 }, 2471, 2496), WEST_DRAGONS(new int[] { 1170, 13079, 30083 }, 2471, 2497), ICE_PLATEAU(new int[] {
			1, 13079, 30083 }, 2471, 2498), VARROCK_WILDERNESS(new int[] { 1170, 13079, 30083 }, 2472, 2494), GREATER_DEMONS(
			new int[] { 1170, 13079, 30083 }, 2472, 2495), LVL_50_PORTALS(new int[] { 1170, 13079, 30083 }, 2472, 2496), RUINS(
			new int[] { 1170, 13079, 30083 }, 2472, 2497), F2P_AREANA(new int[] { 1170, 13079, 30083 }, 2473, 2494), OLD_SCHOOL_AREANA(
			new int[] { 1170, 13079, 30083 }, 2473, 2495), FREMENNIK_SLAYER_DUNGEON(new int[] { 1174, 13061, 30114 }, 2494), SLAYER_TOWER(
			new int[] { 1174, 13061, 30114 }, 2495), FROST_DRAGONS_ICE_PLATEAU(new int[] { 1174, 13061, 30114 }, 2496, 2495), BRIMHAVEN_DUNGEON(
			new int[] { 1174, 13061, 30114 }, 2497), ASGARNIAN_ICE_DUNGEON(new int[] { 1174, 13061, 30114 }, 2498, 2494), TAVERLEY_DUNGEON(
			new int[] { 1174, 13061, 30114 }, 2498, 2495), LIGHT_HOUSE(new int[] { 1174, 13061, 30114 }, 2498, 2496), CHAOS_TUNNELS(
			new int[] { 1174, 13061, 30114 }, 2498, 2497), ANCIENT_CAVERN(new int[] { 1174, 13061, 30114 }, 2498, 2498, 2494), VARROCK(
			new int[] { 1540, 13079, 30162 }, 2494), FALADOR(new int[] { 1540, 13079, 30162 }, 2495), CAMELOT(new int[] { 1540,
			13079, 30162 }, 2496), ARDOUGNE(new int[] { 1540, 13079, 30162 }, 2497), LUMBRIDGE(new int[] { 1540, 13079, 30162 },
			2498), WOODCUTTING(new int[] { 1541, 13069, 30106 }, 2494), FISHING(new int[] { 1541, 13069, 30106 }, 2495), FARMING(
			new int[] { 1541, 13069, 30106 }, 2496), SUMMONING(new int[] { 1541, 13069, 30106 }, 2497), MINNING(new int[] { 1541,
			13069, 30106 }, 2498, 2494), SMITHING(new int[] { 1541, 13069, 30106 }, 2498, 2495), AGILITY(new int[] { 1541, 13069,
			30106 }, 2498, 2496), THIEVING(new int[] { 1541, 13069, 30106 }, 2498, 2497), HUNTER(
			new int[] { 1541, 13069, 30106 }, 2498, 2498), GOD_WARS_DUNGEON(new int[] { 1, 13087, 30138 }, 2494), DAGANNOTH_KINGS(
			new int[] { 7455, 13087, 30138 }, 2495), FROST_DRAGONS_ICE_PATH(new int[] { 7455, 13087, 30138 }, 2496, 2494), TORMENTED_DEMONS(
			new int[] { 7455, 13087, 30138 }, 2497), KBD(new int[] { 7455, 13087, 30138 }, 2498, 2494), NEX(new int[] { 7455,
			13087, 30138 }, 2498, 2495), NOMAD(new int[] { 7455, 13087, 30138 }, 2498, 2496), CORPORAL_BEAST(new int[] { 7455,
			13087, 30138 }, 2498, 2497), CHAOS_ELEMENTAL(new int[] { 7455, 13087, 30138 }, 2498, 2498, 2494), BARRELCHEST_BOSS(
			new int[] { 1, 13087, 30138 }, 2498, 2498, 2495), GLACORS(new int[] { 7455, 13087, 30138 }, 2498, 2498, 2496), AVATAR_OF_DESTRUCTION(
			new int[] { 7455, 13087, 30138 }, 2498, 2498, 2497), KALPHITE_QUEEN(new int[] { 7455, 13087, 30138 }, 2498, 2498,
			2498, 2494), PHOENIX(new int[] { 7455, 13087, 30138 }, 2498, 2498, 2498, 2495), BANDOS_AVATAR(new int[] { 7455,
			13087, 30138 }, 2498, 2498, 2498, 2496), HOME(new int[] { 1195, 12856, 30000 }, 2461), MARKETPLACE(new int[] { 1195,
			12856, 30000 }, 2462);

	private int[] actionIds;
	private int[] spellBookIds;

	Teleports(int[] spellBookIds, int... actionIds) {
		this.actionIds = actionIds;
		this.spellBookIds = spellBookIds;
	}

	public void teleport() {
		int bookId = 0;
		for (int id = 0; id < (actionIds.length + 1); id++) {
			if (id == 0) {
				if (usingLunarSpellBook()) {
					bookId = 2;
				} else if (usingAncientSpellBook()) {
					bookId = 1;
				} else if (usingModernSpellBook()) {
					bookId = 0;
				}
				Menu.sendAction(bookId == 2 ? 646 : 315, 0, 0, spellBookIds[bookId], 0);
				Time.sleep(1200);
			} else {
				Menu.sendAction(315, 0, 0, actionIds[id - 1], 0);
				Time.sleep(1200);
			}
		}
	}

	private boolean usingAncientSpellBook() {
		if (Utils.getTabInterfaceIDs()[6] == 12855) {
			return true;
		}

		return false;
	}

	private boolean usingLunarSpellBook() {
		if (Utils.getTabInterfaceIDs()[6] == 29999) {
			return true;
		}

		return false;
	}

	private boolean usingModernSpellBook() {
		if (Utils.getTabInterfaceIDs()[6] == 1151) {
			return true;
		}

		return false;
	}

}