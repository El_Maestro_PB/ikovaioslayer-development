package com.ikovps.slayer.util;

import java.util.ArrayList;
import java.util.List;

import org.parabot.environment.api.utils.Time;
import org.parabot.environment.scripts.framework.SleepCondition;
import org.rev317.min.api.methods.Game;
import org.rev317.min.api.methods.Players;
import org.rev317.min.api.methods.SceneObjects;
import org.rev317.min.api.wrappers.SceneObject;
import org.rev317.min.api.wrappers.Tile;

/**
 * 
 * Walks a path of teleports, tiles, or sceneobjects.
 * 
 * 
 * 
 * @author Bautista
 * 
 *
 * 
 * @param <T>
 */

public class CustomPath<T> {

	private List<T> path;
	private T currentObject;

	public CustomPath(final T... path) {
		this.path = new ArrayList<T>();
		for (final T object : path) {
			this.path.add(object);
		}
	}

	/**
	 * 
	 * Returns true if the object is a teleport object.
	 * 
	 * 
	 * 
	 * @param object
	 * 
	 * @return boolean
	 */
	private boolean isTeleport(final T object) {
		if (path.isEmpty()) {
			return false;
		}
		return object instanceof Teleports;
	}

	/**
	 * 
	 * Returns true if the object is a sceneobject.
	 * 
	 * 
	 * 
	 * @param object
	 * 
	 * @return boolean
	 */
	private boolean isSceneObject(final T object) {
		if (path.isEmpty()) {
			return false;
		}
		return object instanceof Integer;
	}

	/**
	 * 
	 * Returns true if the object is a tile.
	 * 
	 * 
	 * 
	 * @param object
	 * 
	 * @return boolean
	 */
	private boolean isTile(final T object) {
		if (path.isEmpty()) {
			return false;
		}
		return object instanceof Tile;
	}

	/**
	 * 
	 * Checks if the path has been completely walked.
	 * 
	 * 
	 * 
	 * @return boolean
	 */
	public boolean isWalked() {
		return getLastTile() != null && isOnSamePlane(getLastTile()) && getLastTile().distanceTo() < 4;
	}

	/**
	 * Returns true if the tile is on the same plane as your player.
	 * 
	 * @param tile
	 * @return boolean
	 */
	private boolean isOnSamePlane(final Tile tile) {
		return tile.getPlane() == Players.getMyPlayer().getLocation().getPlane();
	}

	/**
	 * 
	 * Checks if the tile/sceneobject portion of the path is walkable from the
	 * 
	 * players current location.
	 * 
	 * 
	 * 
	 * @return boolean
	 */
	private boolean isWalkable() {
		for (final T pathObject : path) {
			if (isTile(pathObject)) {
				final Tile nextPathTile = (Tile) pathObject;
				if (nextPathTile.distanceTo() <= 6 && isOnSamePlane(nextPathTile)) {
					return true;
				}
			} else if (isSceneObject(pathObject)) {
				final int nextPathSceneObject = ((Integer) pathObject).intValue();
				for (SceneObject sceneObject : SceneObjects.getAllSceneObjects()) {
					if (sceneObject != null) {
						if (sceneObject.getId() == nextPathSceneObject) {
							if (sceneObject.distanceTo() <= 6 && isOnSamePlane(sceneObject.getLocation())) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

	/**
	 * 
	 * Checks if the path can be traversed.
	 * 
	 * 
	 * 
	 * @return boolean
	 */
	public boolean isTraversable() {
		if (path.isEmpty()) {
			return false;
		}
		return currentObject == null ? isTeleport(path.get(0)) : isTeleport(currentObject) || isWalkable();
	}

	/**
	 * 
	 * Gets the next object in the path that has not be interacted with.
	 * 
	 * 
	 * 
	 * @return object
	 */
	private void updateCurrentObject() {
		if (path.get(path.size() - 1).equals(currentObject)) {
			currentObject = null;
		} else if (currentObject == null) {
			currentObject = path.get(0);
		} else {
			currentObject = path.get(path.indexOf(currentObject) + 1);
		}
	}

	public Tile getLastTile() {
		return (Tile) path.get(path.size() - 1);
	}

	/**
	 * 
	 * Walks the entire path.
	 */
	public void traverse() {
		if (Utils.retaliateIsActivated()) {
			Utils.toggleRetaliate();
		}
		while (!isWalked() && isTraversable() && Game.isLoggedIn()) {
			updateCurrentObject();
			if (isWalkable()) {
				if (isTile(currentObject)) {
					final Tile nextTile = (Tile) currentObject;
					if (nextTile != null && nextTile.isOnMinimap() && isOnSamePlane(nextTile)) {
						nextTile.walkTo();
						Time.sleep(new SleepCondition() {
							@Override
							public boolean isValid() {
								return nextTile.distanceTo() <= 3 && Utils.isWalking();
							}
						}, 7000);
					}
				} else if (isSceneObject(currentObject)) {
					final int nextPathSceneObject = ((Integer) currentObject).intValue();
					loop: for (final SceneObject sceneObject : SceneObjects.getAllSceneObjects()) {
						if (sceneObject != null) {
							if (sceneObject.getId() == nextPathSceneObject) {
								if (sceneObject.distanceTo() < 6 && isOnSamePlane(sceneObject.getLocation())) {
									sceneObject.interact(0);
									Time.sleep(5000);
									break loop;
								}
							}
						}
					}
				}
			} else if (isTeleport(currentObject)) {
				((Teleports) currentObject).teleport();
				Time.sleep(3000, 4000);
			} else {
				break;
			}
		}
		currentObject = null;
	}
}