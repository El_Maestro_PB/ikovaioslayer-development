package com.ikovps.slayer.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import org.parabot.core.Context;
import org.parabot.core.asm.ASMClassLoader;
import org.rev317.min.Loader;

public class Reflection {

	public static int[] getTabInterfaceIDs() {
		final int[] i = { 1 };
		try {
			Field tabIDs = Loader.getClient().getClass().getDeclaredField("hA");
			tabIDs.setAccessible(true);
			int[] ids = (int[]) tabIDs.get(Loader.getClient());
			return ids;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return i;
	}

	public static int walking() {
		try {
			Context context = Context.getInstance();
			ASMClassLoader classLoader = context.getASMClassLoader();
			Class<?> entityClass = classLoader.loadClass("aN");
			Object instance = Loader.getClient().getMyPlayer();
			Field walking = entityClass.getDeclaredField("N");
			walking.setAccessible(true);
			return (int) walking.get(instance);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	public static void dropClient() {
		try {
			Method dropClient = Loader.getClient().getClass().getDeclaredMethod("V");
			dropClient.setAccessible(true);
			dropClient.invoke(Loader.getClient());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	


	// public int getCurrentHealth() {
	// try {
	// Context context = Context.getInstance();
	// ASMClassLoader classLoader = context.getASMClassLoader();
	// Class<?> entityClass = classLoader.loadClass("aN");
	// Object instance = entityClass.newInstance();
	// Field currentHealth = entityClass.getDeclaredField("V");
	// currentHealth.setAccessible(true);
	// return (int) currentHealth.get(instance);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// return 0;
	// }

}
