package com.ikovps.slayer.util;

public interface Consumable {

	public void consume();

	public boolean hasInBag();

	public int getCount();

	public boolean needToConsume();

	public String getName();

	public boolean needBankForMore();

}
